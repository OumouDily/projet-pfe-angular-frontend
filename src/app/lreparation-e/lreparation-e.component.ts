import { Component, OnInit, Inject } from '@angular/core';
import { LReparationE } from '../entities/lreparation-e';
import { MAT_DIALOG_DATA,MatDialogRef } from '@angular/material';
import { ReparationExterneService } from '../services/reparation-externe.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-lreparation-e',
  templateUrl: './lreparation-e.component.html',
  styleUrls: ['./lreparation-e.component.css']
})
export class LReparationEComponent implements OnInit {

  formData:LReparationE;
  
  isValid:boolean=true;
  constructor(
        @Inject(MAT_DIALOG_DATA)  public data,
        public dialogRef:MatDialogRef<LReparationEComponent>,
     
        private reparationEService:ReparationExterneService){}


  ngOnInit() {
   
    if(this.data.orderItemIndex==null)
    this.formData={
      Id:null,
      Service:'',
      Montant:0
    }

  


    else
       this.formData=Object.assign({},this.reparationEService.LreparationEList[this.data.orderItemIndex]);
    
  }


 
  onSubmit(form:NgForm){
    if(this.validateForm(form.value)){
      if(this.data.orderItemIndex==null)
    this.reparationEService.LreparationEList.push(form.value);
    else
    this.reparationEService.LreparationEList[this.data.orderItemIndex]=form.value;
    this.dialogRef.close();
  }
}
  validateForm(formData:LReparationE){
    this.isValid=true;
    if(formData.Id==0)
      this.isValid=false;
      else if(formData.Montant==0)
      this.isValid=false;
      return this.isValid;
  }

}
