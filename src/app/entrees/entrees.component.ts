import { Component, OnInit } from '@angular/core';
import { EntreeService } from '../services/entree.service';

@Component({
  selector: 'app-entrees',
  templateUrl: './entrees.component.html',
  styleUrls: ['./entrees.component.css']
})
export class EntreesComponent implements OnInit {

  constructor(private entreeService:EntreeService) { }

  ngOnInit() {
  }

}
