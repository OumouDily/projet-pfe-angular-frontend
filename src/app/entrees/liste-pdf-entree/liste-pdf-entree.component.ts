import { Component, OnInit } from '@angular/core';

import { EntreeService } from 'src/app/services/entree.service';
import { ToastrService } from 'ngx-toastr';
import { Entree } from 'src/app/entities/entree';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-liste-pdf-entree',
  templateUrl: './liste-pdf-entree.component.html',
  styleUrls: ['./liste-pdf-entree.component.css']
})
export class ListePdfEntreeComponent implements OnInit {

  constructor(private entreeService:EntreeService,private toastr:ToastrService) { }

  ngOnInit():void {
    this.entreeService.refreshList();
  
  }
 

  populateForm(f:Entree){
    this.entreeService.formData=Object.assign({},f);
  }
  generatePdf() {
 
    const div = document.getElementById("Entree");
    const options = {background: "white", height: div.clientHeight, width: div.clientWidth};
  
    html2canvas(div, options).then((canvas) => {
  
        
        //Initialize JSPDF
        let doc = new jsPDF("p", "mm", "a4");
     
        //Converting canvas to Image
        let imgData = canvas.toDataURL("image/PNG");
  
        
        //Add image Canvas to PDF
        doc.addImage(imgData, 'PNG',5,0,190,100);
  
        let pdfOutput = doc.output();
        // using ArrayBuffer will allow you to put image inside PDF
        let buffer = new ArrayBuffer(pdfOutput.length);
        let array = new Uint8Array(buffer);
        for (let i = 0; i < pdfOutput.length; i++) {
            array[i] = pdfOutput.charCodeAt(i);
        }
  
        //Name of pdf
        const fileName = "ListeEntree.pdf";
  
        // Make file
        doc.save(fileName);
  
    });
  }

}
