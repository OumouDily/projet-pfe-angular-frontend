import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Entree } from 'src/app/entities/entree';
import { EntreeService } from 'src/app/services/entree.service';
import { EnginService } from 'src/app/services/engin.service';
import { ToastrService } from 'ngx-toastr';
import { MotifService } from 'src/app/services/motif.service';
import { Motif } from 'src/app/entities/motif';
import { Engin } from 'src/app/entities/engin';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-add-entree',
  templateUrl: './add-entree.component.html',
  styleUrls: ['./add-entree.component.css']
})
export class AddEntreeComponent implements OnInit {

  date = new Date();
 motifList:Motif[];
   enginList:Engin[];
   minDate  ;
   
   constructor(private entreeService:EntreeService,private enginService:EnginService,private motifService:MotifService,private toastr:ToastrService,private datePipe: DatePipe) { }
 
   ngOnInit() {
    this.minDate = this.transformDate(new Date());
     this.resetForm();
     this.motifService.getMotifList().then(res => this.motifList = res as Motif[]);
     this.enginService.getEnginList().then(res => this.enginList = res as Engin[]);
     
     
   }
   resetForm(form? : NgForm){
     if(form!=null)
    form.resetForm();
    this.entreeService.formData={
      IdEn:null,
      Numero:Math.floor(100000+Math.random()*900000).toString(),
      Date_Entree:'',
      Id_Engin:0,
      Id_Motif:0,
      Observation:'',
      Index:0,
      Niveau:''
     
    }
   }

   transformDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }
   onSubmit(form: NgForm){
    if(form.value.IdEn==null)
    this.insertEntree(form);
    else
    this.updateEntree(form);
  }
  insertEntree(form: NgForm){
    
    this.entreeService.PostEntree(form.value).subscribe(res => {
    this.toastr.success('Insertion effectuée avec succès!');
  
    this.resetForm(form);
    this.entreeService.refreshList();
    });
  }
  updateEntree(form: NgForm){
    this.entreeService.PutEntree(form.value)
    .subscribe(res => {
      this.toastr.info('Modification effectuée!');
      
      this.resetForm(form);
      this.entreeService.refreshList();
      });
  }
 

}
