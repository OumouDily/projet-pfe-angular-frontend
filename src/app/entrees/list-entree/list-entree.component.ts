import { Component, OnInit } from '@angular/core';
import { EntreeService } from 'src/app/services/entree.service';
import { ToastrService } from 'ngx-toastr';
import { Entree } from 'src/app/entities/entree';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-list-entree',
  templateUrl: './list-entree.component.html',
  styleUrls: ['./list-entree.component.css']
})
export class ListEntreeComponent implements OnInit {

  constructor(private entreeService:EntreeService,private toastr:ToastrService, private datePipe: DatePipe) { }

  ngOnInit() {
    this.entreeService.refreshList();
  }
  populateForm(f:Entree){
    f.Date_Entree=  this.transformDate(f.Date_Entree);
    this.entreeService.formData=Object.assign({},f);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.entreeService.DeleteEntree(id).subscribe(res=>{
      this.entreeService.refreshList();
      this.toastr.warning('Suppression effectuée!');
     
    })
  }
}
transformDate(date) {
  return this.datePipe.transform(date, 'yyyy-MM-dd');
}
}
