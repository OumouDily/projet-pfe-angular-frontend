import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddEnginComponent } from './engins/add-engin/add-engin.component';
import { DatePipe } from '@angular/common';
import { ListEnginComponent } from './engins/list-engin/list-engin.component';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AddPersonneComponent } from './personnes/add-personne/add-personne.component';

import { ListPersonneComponent } from './personnes/list-personne/list-personne.component';
import { AddResidenceComponent } from './residences/add-residence/add-residence.component';

import { ListResidenceComponent } from './residences/list-residence/list-residence.component';

import { AddMarqueComponent } from './marques/add-marque/add-marque.component';

import { ListMarqueComponent } from './marques/list-marque/list-marque.component';
import { AddModelComponent } from './models/add-model/add-model.component';

import { ListModelComponent } from './models/list-model/list-model.component';
import { AddDirectionComponent } from './directions/add-direction/add-direction.component';

import { ListDirectionComponent } from './directions/list-direction/list-direction.component';

import { AddGradeComponent } from './grades/add-grade/add-grade.component';

import { ListGradeComponent } from './grades/list-grade/list-grade.component';
import { AddFonctionComponent } from './fonctions/add-fonction/add-fonction.component';

import { ListFonctionComponent } from './fonctions/list-fonction/list-fonction.component';
import { RouterModule,Routes } from '@angular/router';
import { MarqueService } from './services/marque.service';


import { AddCarburantComponent } from './carburants/add-carburant/add-carburant.component';

import { ListCarburantComponent } from './carburants/list-carburant/list-carburant.component';
import { AddSortieComponent } from './sorties/add-sortie/add-sortie.component';


import { AddEntreeComponent } from './entrees/add-entree/add-entree.component';
import {ChartModule} from 'primeng/chart';

import { CarburantsComponent } from './carburants/carburants.component';
import { DirectionsComponent } from './directions/directions.component';
import { EnginsComponent } from './engins/engins.component';
import { EntreesComponent } from './entrees/entrees.component';
import { FonctionsComponent } from './fonctions/fonctions.component';
import { GradesComponent } from './grades/grades.component';
import { MarquesComponent } from './marques/marques.component';
import { ModelsComponent } from './models/models.component';
import { PersonnesComponent } from './personnes/personnes.component';
import { ResidencesComponent } from './residences/residences.component';
import { SortiesComponent } from './sorties/sorties.component';

import { CarburantService } from './services/carburant.service';
import { DirectionService } from './services/direction.service';
import { EnginService } from './services/engin.service';
import { EntreeService } from './services/entree.service';
import { FonctionService } from './services/fonction.service';
import { GradeService } from './services/grade.service';
import {ReactiveFormsModule} from "@angular/forms";

import { PersonneService } from './services/personne.service';
import { ResidenceService } from './services/residence.service';
import { SortieService } from './services/sortie.service';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FamillesComponent } from './familles/familles.component';
import { AddFamilleComponent } from './familles/add-famille/add-famille.component';
import { ListFamilleComponent } from './familles/list-famille/list-famille.component';
import { ReparationsComponent } from './reparations/reparations.component';
import { TypesComponent } from './types/types.component';
import { TypeEnginsService } from './services/type-engins.service';
import { ListTypeComponent } from './types/list-type/list-type.component';
import { AddTypeComponent } from './types/add-type/add-type.component';
import { LReparationComponent } from './lreparation/lreparation.component';
import { ReparationService } from './services/reparation.service';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSortModule, MatButtonModule, MatFormFieldModule, MatInputModule, MatRippleModule, MatTableModule, MatCardModule, MatSelectModule, MatToolbarModule, MatListModule, MatGridListModule, MatRadioModule, MatCheckboxModule, MatDatepickerModule, MatNativeDateModule, MatSnackBarModule, MatIconModule, MatPaginatorModule} from '@angular/material';
import { ProduitService } from './services/produit.service';
import { MotifsComponent } from './motifs/motifs.component';
import { MotifService } from './services/motif.service';


import { MenuComponent } from './menu/menu.component';



import { ReparationExterneComponent } from './reparation-externe/reparation-externe.component';
import { ReparationExterneService } from './services/reparation-externe.service';
import { LReparationEComponent } from './lreparation-e/lreparation-e.component';
import { FacturesComponent } from './factures/factures.component';
import { AddFactureComponent } from './Factures/add-facture/add-facture.component';
import { ListFactureComponent } from './Factures/list-facture/list-facture.component';
import { FournisseursComponent } from './fournisseurs/fournisseurs.component';
import { AddFournisseurComponent } from './Fournisseurs/add-fournisseur/add-fournisseur.component';
import { ListFournisseurComponent } from './Fournisseurs/list-fournisseur/list-fournisseur.component';
import { FournisseurService } from './services/fournisseur.service';


import { MissionService } from './services/mission.service';
import { DestinationsComponent } from './destinations/destinations.component';
import { DestinationService } from './services/destination.service';
import { AddDestinationComponent } from './destinations/add-destination/add-destination.component';
import { ListDestinationComponent } from './destinations/list-destination/list-destination.component';
import { MissionsComponent } from './missions/missions.component';
import { AddMissionComponent } from './missions/add-mission/add-mission.component';
import { ListMissionComponent } from './missions/list-mission/list-mission.component';

import { DemandeComponent } from './demande/demande.component';
import { AddDemandeComponent } from './demande/add-demande/add-demande.component';
import { ListDemandeComponent } from './demande/list-demande/list-demande.component';
import { LoginComponent } from './login/login.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { RegisterComponent } from './register/register.component';
import { AddMotifComponent } from './motifs/add-motif/add-motif.component';
import { ListMotifComponent } from './motifs/list-motif/list-motif.component';

import { ListepdfMissionComponent } from './missions/listepdf-mission/listepdf-mission.component';
import { ListepdfDemandeComponent } from './demande/listepdf-demande/listepdf-demande.component';
import { ListepdfFactureComponent } from './factures/listepdf-facture/listepdf-facture.component';
import { ListepdfEnginComponent } from './engins/listepdf-engin/listepdf-engin.component';
import { SfamillesComponent } from './sfamilles/sfamilles.component';
import { AddSfamilleComponent } from './Sfamilles/add-sfamille/add-sfamille.component';
import { ListSfamilleComponent } from './Sfamilles/list-sfamille/list-sfamille.component';
import { ReparationListeComponent } from './reparations/reparation-liste/reparation-liste.component';
import { ReparationElisteComponent } from './reparation-externe/reparation-eliste/reparation-eliste.component';

import { AddUserComponent } from './register/add-user/add-user.component';
import { ListUserComponent } from './register/list-user/list-user.component';
import { EntretiensComponent } from './entretiens/entretiens.component';
import { AddEntretienComponent } from './entretiens/add-entretien/add-entretien.component';
import { ListEntretienComponent } from './entretiens/list-entretien/list-entretien.component';
import { ListepdfEntretienComponent } from './entretiens/listepdf-entretien/listepdf-entretien.component';
import { EntretienService } from './services/entretien.service';
import { EnginParcComponent } from './engins/engin-parc/engin-parc.component';
import { AddProduitComponent } from './produits/add-produit/add-produit.component';
import { ListProduitComponent } from './produits/list-produit/list-produit.component';
import { ProduitsComponent } from './produits/produits.component';
import { ListEntreeComponent } from './entrees/list-entree/list-entree.component';
import { ListSortieComponent } from './sorties/list-sortie/list-sortie.component';
import { AuthGuardService } from './services/auth-guard.service';
import { JwtInterceptorService } from './services/jwt-interceptor.service';
import { AuthorityDirective } from './directives/authority.directive';

import { ListePdfSortieComponent } from './sorties/liste-pdf-sortie/liste-pdf-sortie.component';
import { ListePdfEntreeComponent } from './entrees/liste-pdf-entree/liste-pdf-entree.component';
import { ConsommationsComponent } from './consommations/consommations.component';
import { AddConsommationComponent } from './consommations/add-consommation/add-consommation.component';
import { ListConsommationComponent } from './consommations/list-consommation/list-consommation.component';
import { AffectationsComponent } from './affectations/affectations.component';
import { AddAffectationComponent } from './affectations/add-affectation/add-affectation.component';
import { ListAffectationComponent } from './affectations/list-affectation/list-affectation.component';
import { EnginReserveComponent } from './engins/engin-reserve/engin-reserve.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UploadComponent } from './upload/upload.component';
import { ListepdfConsommationComponent } from './consommations/listepdf-consommation/listepdf-consommation.component';
import { ListPdfFournisseurComponent } from './fournisseurs/list-pdf-fournisseur/list-pdf-fournisseur.component';
import { Dashboard1Component } from './dashboard1/dashboard1.component';
import { LoginAdminComponent } from './login-admin/login-admin.component';





@NgModule({
  declarations: [
    AppComponent,
    
    AddEnginComponent,
    ListEnginComponent,
    AddPersonneComponent,
    ListPersonneComponent,
    AddResidenceComponent,
    ListResidenceComponent,
    AddMarqueComponent,
    ListMarqueComponent,
    AddModelComponent,
    ListModelComponent,
    AddDirectionComponent,
    ListDirectionComponent,
    AddGradeComponent,
    ListGradeComponent,
    AddFonctionComponent,
    ListFonctionComponent,
    AddCarburantComponent,
    ListCarburantComponent,
  

    AddEntreeComponent,
    CarburantsComponent,
    DirectionsComponent,
    EnginsComponent,
    EntreesComponent,
    FonctionsComponent,
    GradesComponent,
    MarquesComponent,
    ModelsComponent,
    PersonnesComponent,
   
    FamillesComponent,
    AddFamilleComponent,
    ListFamilleComponent,
    ReparationsComponent,
    TypesComponent,
    AddTypeComponent,
    ListTypeComponent,
    LReparationComponent,
   
    AddFonctionComponent,
    ListFonctionComponent,
    AddGradeComponent,
    ListGradeComponent,
    ReparationsComponent,
    LReparationComponent,
    EntreesComponent,
    AddEntreeComponent,
    MotifsComponent,
    SortiesComponent,
    AddSortieComponent,
    LoginComponent,
    MenuComponent,

    ReparationExterneComponent,
    LReparationEComponent,
    FacturesComponent,
    AddFactureComponent,
    ListFactureComponent,
    FournisseursComponent,
    AddFournisseurComponent,
    ListFournisseurComponent,
    LReparationEComponent,
    DestinationsComponent,
    AddDestinationComponent,
    ListDestinationComponent,
    MissionsComponent,
    AddMissionComponent,
    ListMissionComponent,
   
    DemandeComponent,
    AddDemandeComponent,
    ListDemandeComponent,
    RegisterComponent,
    AddMotifComponent,
    ListMotifComponent,

    ListepdfMissionComponent,

    ListepdfDemandeComponent,

    ListepdfFactureComponent,

    ListepdfEnginComponent,

    SfamillesComponent,

    AddSfamilleComponent,

    ListSfamilleComponent,

    ReparationListeComponent,

    ReparationElisteComponent,



    AddUserComponent,

    ListUserComponent,

    EntretiensComponent,
    AddEntretienComponent,
    ListEntretienComponent,
    ListepdfEntretienComponent,
    EnginParcComponent,
    AddProduitComponent,
    ListProduitComponent,
    ProduitsComponent,
    ListEntreeComponent,
    ListSortieComponent,
    AuthorityDirective,

    ListePdfSortieComponent,
    ListePdfEntreeComponent,
    ConsommationsComponent,
    AddConsommationComponent,
    ListConsommationComponent,
    AffectationsComponent,
    AddAffectationComponent,
    ListAffectationComponent,
    EnginReserveComponent,
    DashboardComponent,
    ResidencesComponent,
    UploadComponent,
    ListepdfConsommationComponent,
    ListPdfFournisseurComponent,
    Dashboard1Component,
    LoginAdminComponent

   

   
    

    
  
  
  ],
  imports: [
    BrowserModule,
    ChartModule,

    AppRoutingModule,
    ToastrModule.forRoot(),
    FormsModule,
   
    HttpModule,
    RouterModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatDialogModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatSelectModule,
    MatTableModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule,
    MatPaginatorModule,
    MatSortModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
     
  ],

  
  entryComponents:[LReparationComponent,LReparationEComponent],


  providers: [DatePipe,{provide: HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi:true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
