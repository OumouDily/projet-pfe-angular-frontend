import { Component, OnInit } from '@angular/core';
import { StatistiqueService } from '../services/statistique.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
   data: any = [];
   data2: any = [];
   private labels2 = [];
  private labels = [];
  private  valuesEssence = [];
  private  valuesGazoil = [];
  private  valuesPlomb = [];
  private values = [];
  constructor(private statistiqueService: StatistiqueService) { }

  ngOnInit() {
    
    this.statistiqueService.getConsomationByMonth().subscribe(result=>{
      
      
      result.forEach(element => {
          
          
        //  this.labels.push(element.Direction );

       
         if( this.labels.includes(element.Direction)){
       
         }else {
          this.labels.push(element.Direction);
          
         }

          
        });


        
        this.labels.forEach(elem => {
          let totalGazoil =0;
          let totalEssence = 0;
          let totalPlomb = 0;
          result.forEach(element => {
           
       
            console.log(elem)
         
          
            if(element.Direction === elem){
              
              if(element.Nature ==='Sans plomb'){
                totalPlomb += element.Litres;
                }
                if(element.Nature ==='Gazoil'){
                  totalGazoil += element.Litres;
                  }
                  if(element.Nature ==='Essence'){
                    totalEssence += element.Litres;
                    }
                    console.log(totalEssence);
                  
            }
           
         
          });

          this.valuesEssence.push(totalEssence);
            this.valuesGazoil.push(totalGazoil);
            this.valuesPlomb.push(totalPlomb);
        });
      
       
        

        this.data = {
          labels: this.labels,
          datasets: [
              {
                  label: 'Sans Plomb',
                  backgroundColor: '#42A5F5',
                  borderColor: '#1E88E5',
                  data: this.valuesPlomb
              },
              {
                  label: 'Gazoil',
                  backgroundColor: '#9CCC65',
                  borderColor: '#7CB342',
                  data: this.valuesGazoil
              },
              {
                label: 'Essence',
                backgroundColor: '#FFCE56',
                borderColor: '#FFCE56',
                data: this.valuesEssence
            }
          ]
      }

    }, ex=>{
      console.log(ex);
    });




    this.statistiqueService.getConsomationByNature().subscribe(result=>{
        result.forEach(element => {
          this.labels2.push(element.Nature);
          this.values.push(element.Litres);

          this.data2 = {
            labels: this.labels2,
            datasets: [
                {
                    data: this.values,
                    backgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ]
                }]    
            };
          
        });
    }, ex=>{
      console.log(ex);
    });
  }

}
