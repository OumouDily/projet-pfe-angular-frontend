import { Component, OnInit } from '@angular/core';
import { DemandeService } from 'src/app/services/demande.service';
import { ToastrService } from 'ngx-toastr';
import { DemandeMission } from 'src/app/entities/demande-mission';



@Component({
  selector: 'app-list-demande',
  templateUrl: './list-demande.component.html',
  styleUrls: ['./list-demande.component.css']
})
export class ListDemandeComponent implements OnInit {

  constructor(private demandeService:DemandeService,private toastr:ToastrService) { }

  ngOnInit() {
    this.demandeService.refreshList();
  }
  populateForm(f:DemandeMission){
    this.demandeService.formData=Object.assign({},f);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.demandeService.DeleteDemandeMission(id).subscribe(res=>{
      this.demandeService.refreshList();
      this.toastr.warning('Suppression effectuée!');
    
    })
  }
}


}
