import { Component, OnInit } from '@angular/core';
import { DestinationService } from 'src/app/services/destination.service';
import { Destination } from 'src/app/entities/destination';
import { ToastrService } from 'ngx-toastr';
import { DemandeService } from 'src/app/services/demande.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-demande',
  templateUrl: './add-demande.component.html',
  styleUrls: ['./add-demande.component.css']
})
export class AddDemandeComponent implements OnInit {

 
  destinationList:Destination[];
  
  constructor(private demandeService:DemandeService,private destinationService:DestinationService,private toastr:ToastrService) { }
 
  ngOnInit() {
    this.resetForm();
  
    this.destinationService.getDestinationList().then(res => this.destinationList = res as Destination[]);
    
  }
  resetForm(form? : NgForm){
    if(form!=null)
   form.resetForm();
   this.demandeService.formData={
     IdDM:null,
     Numero:Math.floor(100000+Math.random()*900000).toString(),
     Date_Demande:'',
     Objet:'',
    Destination:'',
   
   
 
 
    
   }
  }
  onSubmit(form: NgForm){
    if(form.value.IdDM==null)
    this.insertDemande(form);
    else
    this.updateDemande(form);
  }
  insertDemande(form: NgForm){
    this.demandeService.PostDemandeMission(form.value).subscribe(res => {
    this.toastr.success('Insertion effectuée avec succès!');
   // alert("Insertion effectuée avec succès!");
    this.resetForm(form);
    this.demandeService.refreshList();
    });
  }
  updateDemande(form: NgForm){
    this.demandeService.PutDemandeMission(form.value)
    .subscribe(res => {
      this.toastr.info('Modification effectuée!');
      //alert("Modification effectuée!");
      this.resetForm(form);
      this.demandeService.refreshList();
      });
  }

}
