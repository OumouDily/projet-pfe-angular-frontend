import { Component, OnInit } from '@angular/core';
import { DemandeService } from 'src/app/services/demande.service';
import { ToastrService } from 'ngx-toastr';
import { DemandeMission } from 'src/app/entities/demande-mission';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-listepdf-demande',
  templateUrl: './listepdf-demande.component.html',
  styleUrls: ['./listepdf-demande.component.css']
})
export class ListepdfDemandeComponent implements OnInit {

  constructor(private demandeService:DemandeService,private toastr:ToastrService) { }

  ngOnInit() {
    this.demandeService.refreshList();
  }
  populateForm(f:DemandeMission){
    this.demandeService.formData=Object.assign({},f);
  }
 generatePdf() {
 
    const div = document.getElementById("demandeMission");
    const options = {background: "white", height: div.clientHeight, width: div.clientWidth};
  
    html2canvas(div, options).then((canvas) => {
  
        
        //Initialize JSPDF
        let doc = new jsPDF("p", "mm", "a4");
     
        //Converting canvas to Image
        let imgData = canvas.toDataURL("image/PNG");
  
        
        //Add image Canvas to PDF
        doc.addImage(imgData, 'PNG',5,0,190,100);
  
        let pdfOutput = doc.output();
        // using ArrayBuffer will allow you to put image inside PDF
        let buffer = new ArrayBuffer(pdfOutput.length);
        let array = new Uint8Array(buffer);
        for (let i = 0; i < pdfOutput.length; i++) {
            array[i] = pdfOutput.charCodeAt(i);
        }
  
        //Name of pdf
        const fileName = "ListeDemandeMission.pdf";
  
        // Make file
        doc.save(fileName);
  
    });
  }

}
