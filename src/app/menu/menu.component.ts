import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  name = '';
  picture = '';
  constructor(private   userService : UserService, private authenticationService: AuthService) { }

  ngOnInit() {
 this.name = localStorage.getItem('name');
 this.picture = localStorage.getItem('pic');
  }


  public logout() {
    this.authenticationService.logout();
  }


  
}
