import { Component, OnInit } from '@angular/core';
import { EntretienService } from 'src/app/services/entretien.service';
import { ToastrService } from 'ngx-toastr';
import { Entretien } from 'src/app/entities/entretien';

@Component({
  selector: 'app-list-entretien',
  templateUrl: './list-entretien.component.html',
  styleUrls: ['./list-entretien.component.css']
})
export class ListEntretienComponent implements OnInit {

  constructor(private entretienService:EntretienService,private toastr:ToastrService) { }

  ngOnInit() {
    this.entretienService.refreshList();
  }
  populateForm(f:Entretien){
    this.entretienService.formData=Object.assign({},f);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.entretienService.DeleteEntretien(id).subscribe(res=>{
      this.entretienService.refreshList();
      this.toastr.warning('Suppression effectuée!');
    
    })
  }
}


}
