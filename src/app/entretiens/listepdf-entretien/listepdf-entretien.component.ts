import { Component, OnInit } from '@angular/core';
import { EntretienService } from 'src/app/services/entretien.service';
import { ToastrService } from 'ngx-toastr';
import { Entretien } from 'src/app/entities/entretien';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';


@Component({
  selector: 'app-listepdf-entretien',
  templateUrl: './listepdf-entretien.component.html',
  styleUrls: ['./listepdf-entretien.component.css']
})
export class ListepdfEntretienComponent implements OnInit {

  constructor(private entretienService:EntretienService,private toastr:ToastrService) { }

  ngOnInit() {
    this.entretienService.refreshList();
  }
  populateForm(f:Entretien){
    this.entretienService.formData=Object.assign({},f);
  }
 
  generatePdf() {
 
    const div = document.getElementById("Entretien");
    const options = {background: "white", height: div.clientHeight, width: div.clientWidth};
  
    html2canvas(div, options).then((canvas) => {
  
        
        //Initialize JSPDF
        let doc = new jsPDF("p", "mm", "a4");
     
        //Converting canvas to Image
        let imgData = canvas.toDataURL("image/PNG");
  
        
        //Add image Canvas to PDF
        doc.addImage(imgData, 'PNG',5,0,190,100);
  
        let pdfOutput = doc.output();
        // using ArrayBuffer will allow you to put image inside PDF
        let buffer = new ArrayBuffer(pdfOutput.length);
        let array = new Uint8Array(buffer);
        for (let i = 0; i < pdfOutput.length; i++) {
            array[i] = pdfOutput.charCodeAt(i);
        }
  
        //Name of pdf
        const fileName = "ListeEntretien.pdf";
  
        // Make file
        doc.save(fileName);
  
    });
  }
}
