import { Component, OnInit } from '@angular/core';
import { EntretienService } from 'src/app/services/entretien.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { Engin } from 'src/app/entities/engin';
import { EnginService } from 'src/app/services/engin.service';

@Component({
  selector: 'app-add-entretien',
  templateUrl: './add-entretien.component.html',
  styleUrls: ['./add-entretien.component.css']
})
export class AddEntretienComponent implements OnInit {
  enginList:Engin[];

  constructor(private enginService:EnginService,private entretienService:EntretienService,private toastr:ToastrService) { }

  ngOnInit() {
    this.resetForm();
    this.enginService.getEnginList().then(res => this.enginList = res as Engin[]);
  }
  resetForm(form? : NgForm){
    if(form!=null)
   form.resetForm();
   this.entretienService.formData={
     IdEnt:null,
     DateEntretien:'',
     Engin:0,
     Nature:''
    
   }
  }
  onSubmit(form: NgForm){
    if(form.value.IdEnt==null)
    this.insertEntretien(form);
    else
    this.updateEntretien(form);
  }
  insertEntretien(form: NgForm){
    this.entretienService.PostEntretien(form.value).subscribe(res => {
    this.toastr.success('Insertion effectuée avec succès!');
   
    this.resetForm(form);
    this.entretienService.refreshList();
    });
  }
  updateEntretien(form: NgForm){
    this.entretienService.PutEntretien(form.value)
    .subscribe(res => {
      this.toastr.info('Modification effectuée!');
     
      this.resetForm(form);
      this.entretienService.refreshList();
      });
  }
}
