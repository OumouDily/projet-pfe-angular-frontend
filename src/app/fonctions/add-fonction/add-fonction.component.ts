import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';  
import { FormBuilder, FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { FonctionService } from 'src/app/services/fonction.service';
import { Fonction } from 'src/app/entities/fonction';

@Component({
  selector: 'app-add-fonction',
  templateUrl: './add-fonction.component.html',
  styleUrls: ['./add-fonction.component.css']
})
export class AddFonctionComponent implements OnInit {

  constructor(private fonctionService:FonctionService,private toastr:ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }
  resetForm(form? : NgForm){
    if(form!=null)
   form.resetForm();
   this.fonctionService.formData={
     IdF:null,
     LibelleFonction:''
    
   }
  }
  onSubmit(form: NgForm){
    if(form.value.IdF==null)
    this.insertFonction(form);
    else
    this.updateFonction(form);
  }
  insertFonction(form: NgForm){
    this.fonctionService.PostFonction(form.value).subscribe(res => {
    this.toastr.success('Insertion effectuée avec succès!');
   
    this.resetForm(form);
    this.fonctionService.refreshList();
    });
  }
  updateFonction(form: NgForm){
    this.fonctionService.PutFonction(form.value)
    .subscribe(res => {
      this.toastr.info('Modification effectuée!');
     
      this.resetForm(form);
      this.fonctionService.refreshList();
      });
  }


}
