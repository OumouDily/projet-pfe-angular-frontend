import { Component, OnInit } from '@angular/core';
import { FonctionService } from 'src/app/services/fonction.service';
import { ToastrService } from 'ngx-toastr';
import { Fonction } from 'src/app/entities/fonction';

@Component({
  selector: 'app-list-fonction',
  templateUrl: './list-fonction.component.html',
  styleUrls: ['./list-fonction.component.css']
})
export class ListFonctionComponent implements OnInit {

  constructor(private fonctionService:FonctionService,private toastr:ToastrService) { }

  ngOnInit() {
    this.fonctionService.refreshList();
  }
  populateForm(f:Fonction){
    this.fonctionService.formData=Object.assign({},f);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.fonctionService.DeleteFonction(id).subscribe(res=>{
      this.fonctionService.refreshList();
      this.toastr.warning('Suppression effectuée!');
      
    })
  }
}

}
