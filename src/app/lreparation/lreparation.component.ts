import { Component, OnInit, Inject } from '@angular/core';
import { LReparation } from '../entities/l-reparation';
import { Produit } from '../entities/produit';
import { MAT_DIALOG_DATA,MatDialogRef } from '@angular/material';
import { NgForm } from '@angular/forms';
import { ProduitService } from '../services/produit.service';
import { ReparationService } from '../services/reparation.service';

@Component({
  selector: 'app-lreparation',
  templateUrl: './lreparation.component.html',
  styleUrls: ['./lreparation.component.css']
})
export class LReparationComponent implements OnInit {

  formData:LReparation;
  List:Produit[];
  isValid:boolean=true;
  constructor(
   
        @Inject(MAT_DIALOG_DATA)  public data,
        public dialogRef:MatDialogRef<LReparationComponent>,
        private produitService:ProduitService,
        private reparationService:ReparationService){}


  ngOnInit() {
    this.produitService.getProduitList().then(res => this.List = res as Produit[]);
    if(this.data.lReparationIndex==null)
    this.formData={
      Id:null,
      IdReparation:this.data.IdReparation,
      IdP:0,
      ProduitName:'',
      PU:0,
      Quantite:0,
      Montant:0
    }

   


    else
       this.formData=Object.assign({},this.reparationService.LreparationList[this.data.lReparationIndex]);
    
  }
  updatePrice(ctrl){
    if(ctrl.selectedIndex==0){
      this.formData.PU=0;
      this.formData.ProduitName= '';
    }
    else{
      this.formData.PU=this.List[ctrl.selectedIndex-1].PU;
      this.formData.ProduitName=this.List[ctrl.selectedIndex-1].IdProduit;
     // this.formData.Id_Produit=0;
    }
    this.updateTotal();
  }

  updateTotal(){
    this.formData.Montant=parseFloat((this.formData.Quantite*this.formData.PU).toFixed(2));
  }
 
  onSubmit(form:NgForm){
    if(this.validateForm(form.value)){
     
      if(this.data.lReparationIndex==null)
     
    this.reparationService.LreparationList.push(form.value);
   
    else
    this.reparationService.LreparationList[this.data.lReparationIndex]=form.value;
    this.dialogRef.close();
  }
}

  validateForm(formData:LReparation){
    this.isValid=true;
    if(formData.IdP==0)
      this.isValid=false;
      else if(formData.Quantite ==0)
      this.isValid=false;
      return this.isValid;
  }

}
