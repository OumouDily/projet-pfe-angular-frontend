import { Component, OnInit } from '@angular/core';
import { MotifService } from 'src/app/services/motif.service';
import { ToastrService } from 'ngx-toastr';
import { Motif } from 'src/app/entities/motif';

@Component({
  selector: 'app-list-motif',
  templateUrl: './list-motif.component.html',
  styleUrls: ['./list-motif.component.css']
})
export class ListMotifComponent implements OnInit {

  constructor(private motifService:MotifService,private toastr:ToastrService) { }
  
  ngOnInit() {
    this.motifService.refreshList();

    

  }
 
  

  populateForm(f:Motif){
    this.motifService.formData=Object.assign({},f);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.motifService.DeleteMotif(id).subscribe(res=>{
      this.motifService.refreshList();
      this.toastr.warning('Suppression effectuée!');
    
    })
  }
}

}
