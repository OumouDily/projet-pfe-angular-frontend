import { Component, OnInit } from '@angular/core';
import { MotifService } from 'src/app/services/motif.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-motif',
  templateUrl: './add-motif.component.html',
  styleUrls: ['./add-motif.component.css']
})
export class AddMotifComponent implements OnInit {

 
 
 constructor(private motifService:MotifService,private toastr:ToastrService) { }

 ngOnInit() {
   this.resetForm();

   
 }
 resetForm(form? : NgForm){
   if(form!=null)
  form.resetForm();
  this.motifService.formData={
    Id:null,
   LibelleMotif:''
  


   
  }
 }
 onSubmit(form: NgForm){
   if(form.value.Id==null)
   this.insertMotif(form);
   else
   this.updateMotif(form);
 }
 insertMotif(form: NgForm){
   this.motifService.PostMotif(form.value).subscribe(res => {
   this.toastr.success('Insertion effectuée avec succès!');
 
   this.resetForm(form);
   this.motifService.refreshList();
   });
 }
 updateMotif(form: NgForm){
   this.motifService.PutMotif(form.value)
   .subscribe(res => {
     this.toastr.info('Modification effectuée!');
     
     this.resetForm(form);
     this.motifService.refreshList();
     });
 }

}
