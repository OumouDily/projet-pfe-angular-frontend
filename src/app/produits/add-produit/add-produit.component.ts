import { Component, OnInit } from '@angular/core';
import { Famille } from 'src/app/entities/famille';
import { Sfamille } from 'src/app/entities/sfamille';
import { FamilleService } from 'src/app/services/famille.service';
import { SfamilleService } from 'src/app/services/sfamille.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { ProduitService } from 'src/app/services/produit.service';

@Component({
  selector: 'app-add-produit',
  templateUrl: './add-produit.component.html',
  styleUrls: ['./add-produit.component.css']
})
export class AddProduitComponent implements OnInit {

  listfam:Famille[];
  listsfam:Sfamille[];
 
   constructor(private familleService:FamilleService,private sfamilleService:SfamilleService,private produitService:ProduitService,private toastr:ToastrService) { }
 
   ngOnInit() {
     if(this.produitService.formData.IdP == null)
     {
     this.resetForm();
   }
     this.familleService.getFamilleList().then(res => this.listfam = res as Famille[]);
     this.sfamilleService.getSfamilleList().then(res => this.listsfam = res as Sfamille[]);
    
    
   }
   resetForm(form? : NgForm){
     if(form!=null)
    form.resetForm();
    this.produitService.formData={
      IdP:null,
      IdProduit:'',
    
     Design:'',
     PU:0,
  
      TVA:0,
      Fodec:0,
      Strock_Init:'',
      Id_S_Famille:0,
      Id_Famille:0,
      Quantite:0,
      Montant:0
 
 
     
    }
   }
   onSubmit(form: NgForm){
     if(form.value.IdP==null)
     this.insertProuidt(form);
     else
     this.updateProduit(form);
   }
   insertProuidt(form: NgForm){
     this.produitService.PostProduit(form.value).subscribe(res => {
     this.toastr.success('Insertion effectuée avec succès!');
   
     this.resetForm(form);
     this.produitService.refreshList();
     });
   }
   updateProduit(form: NgForm){
     this.produitService.PutProduit(form.value)
     .subscribe(res => {
       this.toastr.info('Modification effectuée!');
      
       this.resetForm(form);
       this.produitService.refreshList();
       });
   }
}
