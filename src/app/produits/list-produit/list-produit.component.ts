import { Component, OnInit } from '@angular/core';
import { ProduitService } from 'src/app/services/produit.service';
import { ToastrService } from 'ngx-toastr';
import { Produit } from 'src/app/entities/produit';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-produit',
  templateUrl: './list-produit.component.html',
  styleUrls: ['./list-produit.component.css']
})
export class ListProduitComponent implements OnInit {

  constructor(private produitService:ProduitService,private toastr:ToastrService, private router: Router) { }

  ngOnInit() {
    this.produitService.refreshList();
  }
  populateForm(f:Produit){
    this.produitService.formData=Object.assign({},f);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.produitService.DeleteProduit(id).subscribe(res=>{
      this.produitService.refreshList();
      this.toastr.warning('Suppression effectuée!');
     
    })
  }
}

selectProduit(f :Produit){
  
  this.produitService.formData = f;
 
  this.router.navigate(['/add-produit']);
}
}
