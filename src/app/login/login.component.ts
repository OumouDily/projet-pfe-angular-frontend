import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { User } from '../entities/user';
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

 error='';
  rootURL: string;

  User: any = {};
  constructor(private   userService : UserService,private router: Router, 
    private authenticationService: AuthService) { }




  ngOnInit() {
    this.userService.isLogin = false;
  }


  authentifier() {
  
    this.authenticationService.login(this.User).subscribe( res => {
      localStorage.setItem('id', res.Id);
      localStorage.setItem('pic', res.Picture);
      localStorage.setItem('name', res.Nom);
      this.authenticationService.storeToekn(res.Token);
      this.router.navigate(['/dashboard']);
      

    }, ex => {
      this.error ="Erreur d'authentification: Merci de verifier votre login ou mot de passe"
      console.log(ex);
    });
  }
 

 
  
}
