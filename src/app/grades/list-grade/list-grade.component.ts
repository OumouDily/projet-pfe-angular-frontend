import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { GradeService } from 'src/app/services/grade.service';
import { Grade } from 'src/app/entities/grade';

@Component({
  selector: 'app-list-grade',
  templateUrl: './list-grade.component.html',
  styleUrls: ['./list-grade.component.css']
})
export class ListGradeComponent implements OnInit {

  constructor(private gradeService:GradeService,private toastr:ToastrService) { }

  ngOnInit() {
    this.gradeService.refreshList();
  }
  populateForm(g:Grade){
    this.gradeService.formData=Object.assign({},g);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.gradeService.DeleteGrade(id).subscribe(res=>{
      this.gradeService.refreshList();
      this.toastr.warning('Suppression effectuée!');
      
    })
  }
}

}
