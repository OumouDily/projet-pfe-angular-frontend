import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';  
import { FormBuilder, FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { GradeService } from 'src/app/services/grade.service';
import { Grade } from 'src/app/entities/grade';

@Component({
  selector: 'app-add-grade',
  templateUrl: './add-grade.component.html',
  styleUrls: ['./add-grade.component.css']
})
export class AddGradeComponent implements OnInit {

  constructor(private gradeService:GradeService,private toastr:ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }
  resetForm(form? : NgForm){
    if(form!=null)
   form.resetForm();
   this.gradeService.formData={
     IdG:null,
     LibelleGrade:''
    
   }
  }
  onSubmit(form: NgForm){
    if(form.value.IdG==null)
    this.insertGrade(form);
    else
    this.updateGrade(form);
  }
  insertGrade(form: NgForm){
    this.gradeService.PostGrade(form.value).subscribe(res => {
    this.toastr.success('Insertion effectuée avec succès!');
   
    this.resetForm(form);
    this.gradeService.refreshList();
    });
  }
  updateGrade(form: NgForm){
    this.gradeService.PutGrade(form.value)
    .subscribe(res => {
    this.toastr.info('Modification effectuée!');
     
      this.resetForm(form);
      this.gradeService.refreshList();
      });
  }


}
