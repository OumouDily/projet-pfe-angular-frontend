import { Component, OnInit } from '@angular/core';
import { ConsommationService } from 'src/app/services/consommation.service';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { Consommation } from 'src/app/entities/consommation';

@Component({
  selector: 'app-list-consommation',
  templateUrl: './list-consommation.component.html',
  styleUrls: ['./list-consommation.component.css']
})
export class ListConsommationComponent implements OnInit {

  constructor(private consommationService:ConsommationService,private toastr:ToastrService, private datePipe: DatePipe) { }

  ngOnInit() {
    this.consommationService.refreshList();
  }
  populateForm(f:Consommation){
    f.Date_nvt=  this.transformDate(f.Date_nvt);
    this.consommationService.formData=Object.assign({},f);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.consommationService.DeleteConsommation(id).subscribe(res=>{
      this.consommationService.refreshList();
      this.toastr.warning('Suppression effectuée!');
     
    })
  }
}
transformDate(date) {
  return this.datePipe.transform(date, 'yyyy-MM-dd');
}

}
