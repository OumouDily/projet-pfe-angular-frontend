import { Component, OnInit } from '@angular/core';
import { Direction } from '@angular/cdk/bidi';
import { DirectionService } from 'src/app/services/direction.service';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { ConsommationService } from 'src/app/services/consommation.service';
import { Consommation } from 'src/app/entities/consommation';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-consommation',
  templateUrl: './add-consommation.component.html',
  styleUrls: ['./add-consommation.component.css']
})
export class AddConsommationComponent implements OnInit {

   
  directionList:Direction[];

  
  minDate  ;
  
  constructor(private directionService:DirectionService,private consommationService:ConsommationService,
    private toastr:ToastrService,private datePipe: DatePipe) { }

  ngOnInit() {
   this.minDate = this.transformDate(new Date());
    this.resetForm();
    this.directionService.getDirectionList().then(res => this.directionList = res as Direction[]);
   
   
  }

  transformDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }
  resetForm(form? : NgForm){
    if(form!=null)
   form.resetForm();
   this.consommationService.formData={
     Id:null,
    Id_Direction:0,
    Observation:'',
     Date_nvt:'',
     NbreL:0,
     Nature:''


    
   }
  }
  onSubmit(form: NgForm){
    if(form.value.IdAff==null)
    this.insertConsommation(form);
    else
    this.updateConsommation(form);
  }
  insertConsommation(form: NgForm){
    this.consommationService.PostConsommation(form.value).subscribe(res => {
    this.toastr.success('Insertion effectuée avec succès!');
  
    this.resetForm(form);
    this.consommationService.refreshList();
    });
  }
  updateConsommation(form: NgForm){
    this.consommationService.PutConsommation(form.value)
    .subscribe(res => {
      this.toastr.info('Modification effectuée!');
    
      this.resetForm(form);
      this.consommationService.refreshList();
      });
  }



}
