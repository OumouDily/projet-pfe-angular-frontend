import { Component, OnInit } from '@angular/core';
import { FamilleService } from 'src/app/services/famille.service';
import { ToastrService } from 'ngx-toastr';
import { Famille } from 'src/app/entities/famille';

@Component({
  selector: 'app-list-famille',
  templateUrl: './list-famille.component.html',
  styleUrls: ['./list-famille.component.css']
})
export class ListFamilleComponent implements OnInit {

  constructor(private familleService:FamilleService,private toastr:ToastrService) { }

  ngOnInit() {
    this.familleService.refreshList();
  }
  populateForm(f:Famille){
    this.familleService.formData=Object.assign({},f);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.familleService.DeleteFamille(id).subscribe(res=>{
      this.familleService.refreshList();
      this.toastr.warning('Suppression effectuée!');
     
    })
  }
}
}
