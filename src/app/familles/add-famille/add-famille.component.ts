import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { FamilleService } from 'src/app/services/famille.service';

@Component({
  selector: 'app-add-famille',
  templateUrl: './add-famille.component.html',
  styleUrls: ['./add-famille.component.css']
})
export class AddFamilleComponent implements OnInit {

  constructor(private familleService:FamilleService,private toastr:ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }
  resetForm(form? : NgForm){
    if(form!=null)
   form.resetForm();
   this.familleService.formData={
     IdFa:null,
     LibelleFamille:''
    
   }
  }
  onSubmit(form: NgForm){
    if(form.value.IdFa==null)
    this.insertFamille(form);
    else
    this.updateFamille(form);
  }
  insertFamille(form: NgForm){
    this.familleService.PostFamille(form.value).subscribe(res => {
   this.toastr.success('Insertion effectuée avec succès!');
   
    this.resetForm(form);
    this.familleService.refreshList();
    });
  }
  updateFamille(form: NgForm){
    this.familleService.PutFamille(form.value)
    .subscribe(res => {
     this.toastr.info('Modification effectuée!');

      this.resetForm(form);
      this.familleService.refreshList();
      });
  }

}
