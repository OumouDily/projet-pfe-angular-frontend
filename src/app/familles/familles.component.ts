import { Component, OnInit } from '@angular/core';
import { FamilleService } from '../services/famille.service';

@Component({
  selector: 'app-familles',
  templateUrl: './familles.component.html',
  styleUrls: ['./familles.component.css']
})
export class FamillesComponent implements OnInit {

  constructor(private familleService:FamilleService) { }

  ngOnInit() {
  }

}
