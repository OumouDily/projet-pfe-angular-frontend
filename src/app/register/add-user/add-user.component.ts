import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  patternPwd = '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$';
  patternLogin='^[a-z0-9_-]{8,15}$';
  patternPhone='^((\\+91-?)|0)?[0-9]{10}$';
  constructor(private userService: UserService,private toastr:ToastrService,private router: Router
    ) { }

ngOnInit() {
this.resetForm();
}
resetForm(form? : NgForm){
if(form!=null)
form.resetForm();
this.userService.formData={
Id:null,
Matricule:'',

Nom:'',

Tel:0,
Login:'',
Mdp:'',
Role:''


}
}

onSubmit(form: NgForm){
if(form.value.Id==null)
this.insertUser(form);
}
insertUser(form: NgForm){
this.userService.PostUser(form.value).subscribe(res => {
this.toastr.success('Insertion effectuée avec succès!');

this.resetForm(form);
this.userService.refreshList();
this.router.navigate(['/Login']);
});
}

}
