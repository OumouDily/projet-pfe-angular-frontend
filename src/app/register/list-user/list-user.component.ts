import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ToastrService } from 'ngx-toastr';
import { User } from 'src/app/entities/user';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  constructor(private userService:UserService,private toastr:ToastrService) { }

  ngOnInit() {
    this.userService.refreshList();
  }
  populateForm(u:User){
    this.userService.formData=Object.assign({},u);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.userService.DeleteUser(id).subscribe(res=>{
      this.userService.refreshList();
      this.toastr.warning('Suppression effectuée!');
     
    })
  }
}
}
