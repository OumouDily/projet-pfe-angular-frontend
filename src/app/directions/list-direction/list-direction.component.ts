import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Direction } from 'src/app/entities/direction';
import { DirectionService } from 'src/app/services/direction.service';

@Component({
  selector: 'app-list-direction',
  templateUrl: './list-direction.component.html',
  styleUrls: ['./list-direction.component.css']
})
export class ListDirectionComponent implements OnInit {

  constructor(private directionService:DirectionService,private toastr:ToastrService) { }

  ngOnInit() {
    this.directionService.refreshList();
  }
  populateForm(d:Direction){
    this.directionService.formData=Object.assign({},d);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.directionService.DeleteDirection(id).subscribe(res=>{
      this.directionService.refreshList();
      this.toastr.warning('Suppression effectuée!');
    
    })
  }
}

}
