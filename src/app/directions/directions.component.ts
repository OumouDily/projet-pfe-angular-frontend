import { Component, OnInit } from '@angular/core';
import { DirectionService } from '../services/direction.service';

@Component({
  selector: 'app-directions',
  templateUrl: './directions.component.html',
  styleUrls: ['./directions.component.css']
})
export class DirectionsComponent implements OnInit {

  constructor(private directionService:DirectionService) { }

  ngOnInit() {
  }

}
