import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';  
import { FormBuilder, FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { DirectionService } from 'src/app/services/direction.service';
import { Direction } from 'src/app/entities/direction';

@Component({
  selector: 'app-add-direction',
  templateUrl: './add-direction.component.html',
  styleUrls: ['./add-direction.component.css']
})
export class AddDirectionComponent implements OnInit {

  constructor(private directionService:DirectionService,private toastr:ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }
  resetForm(form? : NgForm){
    if(form!=null)
   form.resetForm();
   this.directionService.formData={
     IdD:null,
     LibelleDirection:''
    
   }
  }
  onSubmit(form: NgForm){
    if(form.value.IdD==null)
    this.insertDirection(form);
    else
    this.updateDirection(form);
  }
  insertDirection(form: NgForm){
    this.directionService.PostDirection(form.value).subscribe(res => {
    this.toastr.success('Insertion effectuée avec succès!');
    
    this.resetForm(form);
    this.directionService.refreshList();
    });
  }
  updateDirection(form: NgForm){
    this.directionService.PutDirection(form.value)
    .subscribe(res => {
     this.toastr.info('Modification effectuée!');
     
      this.resetForm(form);
      this.directionService.refreshList();
      });
  }

}
