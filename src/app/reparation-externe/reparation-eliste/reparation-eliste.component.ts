import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ReparationExterneService } from 'src/app/services/reparation-externe.service';

@Component({
  selector: 'app-reparation-eliste',
  templateUrl: './reparation-eliste.component.html',
  styleUrls: ['./reparation-eliste.component.css']
})
export class ReparationElisteComponent implements OnInit {

  reparationEList;
  constructor(private service:ReparationExterneService,private router:Router, private toastr: ToastrService) { }

  ngOnInit() {
    this.refreshList();
  }
  refreshList(){
    this.service.getReparationList().then(res => this.reparationEList = res);
  }
  openForEdit(Id:number){
this.router.navigate(['/reparationE/modification/'+Id]);
  }
  onReparationDelete(id:number){
    if(confirm('Etes-vous sur de vouloir supprimer?')){
this.service.deleteReparation(id).then(res=>{
  this.refreshList();
  this.toastr.warning('Suppression effectuée avec succès!');
});
  }
}

}
