import { Component, OnInit } from '@angular/core';
import { Engin } from '../entities/engin';
import { EnginService } from '../services/engin.service';
import { ReparationExterneService } from '../services/reparation-externe.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { LReparationEComponent } from '../lreparation-e/lreparation-e.component';
import { ToastrService } from 'ngx-toastr';
import { LReparation } from '../entities/l-reparation';
import { Reparation } from '../entities/reparation';
import { Fournisseur } from '../entities/fournisseur';
import { FournisseurService } from '../services/fournisseur.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-reparation-externe',
  templateUrl: './reparation-externe.component.html',
  styleUrls: ['./reparation-externe.component.css']
})
export class ReparationExterneComponent implements OnInit {

  enginList:Engin[];
  fournisseurList:Fournisseur[];
  isValid:boolean=true;
  minDate  ;

  constructor(private fournisseurService: FournisseurService,private toastr: ToastrService,private service:ReparationExterneService,private enginService:EnginService,private dialog:MatDialog,private router:Router,private currentRoute:ActivatedRoute,private datePipe: DatePipe) { }

  ngOnInit() {
    this.minDate = this.transformDate(new Date());
    let Id=this.currentRoute.snapshot.paramMap.get('id');
    if(Id==null)
    this.resetForm();
    else{
      this.service.getReparationByID(parseInt(Id)).then(res=>{
      this.service.formData=res.reparationE;
      this.service.LreparationEList=res.reparationEDetails;
      });
    }
    
    this.fournisseurService.getFournisseurList().then(res => this.fournisseurList = res as Fournisseur[]);
    this.enginService.getEnginList().then(res => this.enginList = res as Engin[]);
  }

  resetForm(form?:NgForm){
    if(form = null)
    form.resetForm();
    this.service.formData={
      IdRE:null,
      Fournisseur:0,
      Id_Engin:0,
      DateR:'',
      Total:0,
      DeletedReparationId:''
    };
   this.service.LreparationEList=[];
  }
  transformDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }

  AddOrEdit(orderItemIndex,IdRE){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width="50%";
    dialogConfig.data={orderItemIndex,IdRE};

    this.dialog.open(LReparationEComponent, dialogConfig).afterClosed().subscribe(res=>{
      this.updateGrandTotal();
    });

  }
  onDelete(Id:number,i:number){
    if(Id!=null)
    this.service.formData.DeletedReparationId+=Id+",";
   this.service.LreparationEList.splice(i,1); 
  this.updateGrandTotal();
  }

  updateGrandTotal(){
    
    this.service.formData.Total=this.service.LreparationEList.reduce((prev,curr)=>{
      return parseInt(prev.toString())+parseInt(curr.Montant.toString());
     
    },0);
    this.service.formData.Total=parseFloat(this.service.formData.Total.toFixed(2));
  }
  validateForm(){
    this.isValid=true;
    if(this.service.formData.Id_Engin==0)
    this.isValid=false;
    else if(this.service.LreparationEList.length==0)
    this.isValid=false;
    return this.isValid;
  }
  onSubmit(form:NgForm){
   if(this.validateForm()){
    this.service.saveOrUpdate().subscribe(res=>{
  this.resetForm();
  this.toastr.success('Insertion effectuée avec succès!');
  this.router.navigate(['/ReparationExternes']);
    })
      
   }
  }

}
