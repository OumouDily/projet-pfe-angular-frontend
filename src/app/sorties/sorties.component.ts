import { Component, OnInit } from '@angular/core';
import { SortieService } from '../services/sortie.service';

@Component({
  selector: 'app-sorties',
  templateUrl: './sorties.component.html',
  styleUrls: ['./sorties.component.css']
})
export class SortiesComponent implements OnInit {

  constructor(private sortieService:SortieService) { }

  ngOnInit() {
  }

}
