import { Component, OnInit } from '@angular/core';
import { EntreeService } from 'src/app/services/entree.service';
import { EnginService } from 'src/app/services/engin.service';
import { Engin } from 'src/app/entities/engin';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { SortieService } from 'src/app/services/sortie.service';
import { Entree } from 'src/app/entities/entree';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-add-sortie',
  templateUrl: './add-sortie.component.html',
  styleUrls: ['./add-sortie.component.css']
})
export class AddSortieComponent implements OnInit {
  
 
 date = new Date();
 
  entreeList:Entree[];
  enginList:Engin[];
  minDate  ;
  
  constructor(private sortieService:SortieService,private entreeService:EntreeService,private enginService:EnginService,private toastr:ToastrService,private datePipe: DatePipe) { }

  ngOnInit() {
    this.minDate = this.transformDate(new Date());
    this.resetForm();
    this.entreeService.getEntreeList().then(res => this.entreeList = res as Entree[]);
    this.enginService.getEnginList().then(res => this.enginList = res as Engin[]);
    
    
  }
  resetForm(form? : NgForm){
    if(form!=null)
   form.resetForm();
   this.sortieService.formData={
     Id:null,
     NumeroS:Math.floor(100000+Math.random()*900000).toString(),
     //Date_Entree:"dd/MM/yyyy",
     Date_Sortie : '',
     Numero_Entree:0,
     Id_Engin:0,
     Observation:''
    
    
   }
  }

  transformDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }
  onSubmit(form: NgForm){
   if(form.value.Id==null)
   this.insertSortie(form);
   else
   this.updateSortie(form);
 }
 insertSortie(form: NgForm){
   
   this.sortieService.PostSortie(form.value).subscribe(res => {
   this.toastr.success('Insertion effectuée avec succès!');
 
   this.resetForm(form);
   this.sortieService.refreshList();
   });
 }
 updateSortie(form: NgForm){
   this.sortieService.PutSortie(form.value)
   .subscribe(res => {
     this.toastr.info('Modification effectuée!');
     
     this.resetForm(form);
     this.sortieService.refreshList();
     });
 }


}
