import { Component, OnInit } from '@angular/core';
import { SortieService } from 'src/app/services/sortie.service';
import { ToastrService } from 'ngx-toastr';
import { Sortie } from 'src/app/entities/sortie';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-liste-pdf-sortie',
  templateUrl: './liste-pdf-sortie.component.html',
  styleUrls: ['./liste-pdf-sortie.component.css']
})
export class ListePdfSortieComponent implements OnInit {

  constructor(private sortieService:SortieService,private toastr:ToastrService) { }

  ngOnInit():void {
    this.sortieService.refreshList();
  
  }
 

  populateForm(f:Sortie){
    this.sortieService.formData=Object.assign({},f);
  }
 generatePdf() {
 
    const div = document.getElementById("Sortie");
    const options = {background: "white", height: div.clientHeight, width: div.clientWidth};
  
    html2canvas(div, options).then((canvas) => {
  
        
        //Initialize JSPDF
        let doc = new jsPDF("p", "mm", "a4");
     
        //Converting canvas to Image
        let imgData = canvas.toDataURL("image/PNG");
  
        
        //Add image Canvas to PDF
        doc.addImage(imgData, 'PNG',5,0,190,100);
  
        let pdfOutput = doc.output();
        // using ArrayBuffer will allow you to put image inside PDF
        let buffer = new ArrayBuffer(pdfOutput.length);
        let array = new Uint8Array(buffer);
        for (let i = 0; i < pdfOutput.length; i++) {
            array[i] = pdfOutput.charCodeAt(i);
        }
  
        //Name of pdf
        const fileName = "ListeSortie.pdf";
  
        // Make file
        doc.save(fileName);
  
    });
  }
}
