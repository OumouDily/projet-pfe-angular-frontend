import { Component, OnInit } from '@angular/core';
import { SortieService } from 'src/app/services/sortie.service';
import { ToastrService } from 'ngx-toastr';
import { Sortie } from 'src/app/entities/sortie';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-list-sortie',
  templateUrl: './list-sortie.component.html',
  styleUrls: ['./list-sortie.component.css']
})
export class ListSortieComponent implements OnInit {
  
  constructor(private sortieService:SortieService,private toastr:ToastrService,
    private datePipe: DatePipe) { }

  ngOnInit() {
    this.sortieService.refreshList();
  }
  populateForm(f:Sortie){
    f.Date_Sortie =  this.transformDate(f.Date_Sortie);
    this.sortieService.formData=Object.assign({},f);
    console.log(this.sortieService.formData);
  }

  transformDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.sortieService.DeleteSortie(id).subscribe(res=>{
      this.sortieService.refreshList();
      this.toastr.warning('Suppression effectuée!');
     
    })
  }
}

}
