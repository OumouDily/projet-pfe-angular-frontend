import { Component, OnInit } from '@angular/core';
import { Affectation } from 'src/app/entities/affectation';
import { Personne } from 'src/app/entities/personne';
import { Engin } from 'src/app/entities/engin';
import { PersonneService } from 'src/app/services/personne.service';
import { EnginService } from 'src/app/services/engin.service';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { NgForm } from '@angular/forms';
import { AffectationService } from 'src/app/services/affectation.service';

@Component({
  selector: 'app-add-affectation',
  templateUrl: './add-affectation.component.html',
  styleUrls: ['./add-affectation.component.css']
})
export class AddAffectationComponent implements OnInit {

  
  personneList:Personne[];
  enginList:Engin[];
  
  minDate  ;
  
  constructor(private personneService:PersonneService,private enginService:EnginService,
private affectationService:AffectationService,
    private toastr:ToastrService,private datePipe: DatePipe) { }

  ngOnInit() {
   this.minDate = this.transformDate(new Date());
    this.resetForm();
    this.personneService.getPersonneList().then(res => this.personneList = res as Personne[]);
    this.enginService.getEnginList().then(res => this.enginList = res as Engin[]);
   
  }

  transformDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }
  resetForm(form? : NgForm){
    if(form!=null)
   form.resetForm();
   this.affectationService.formData={
     IdAff:null,
    dateAff:'',
     Id_Engin:0,
    Id_Personne:0,
    Observation:'',
     TypeAff:'',
     Etat_Voiture:'',
     Index:0,
     RoueSecours:'',
     AllumeGaz:'',
    Extincteur:''


    
   }
  }
  onSubmit(form: NgForm){
    if(form.value.IdAff==null)
    this.insertAffectation(form);
    else
    this.updateAffectation(form);
  }
  insertAffectation(form: NgForm){
    this.affectationService.PostAffectation(form.value).subscribe(res => {
    this.toastr.success('Insertion effectuée avec succès!');
  
    this.resetForm(form);
    this.affectationService.refreshList();
    });
  }
  updateAffectation(form: NgForm){
    this.affectationService.PutAffectation(form.value)
    .subscribe(res => {
      this.toastr.info('Modification effectuée!');
    
      this.resetForm(form);
      this.affectationService.refreshList();
      });
  }


}
