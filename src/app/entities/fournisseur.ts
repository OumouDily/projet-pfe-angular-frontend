export class Fournisseur {
    IdFo:number;
    LibelleFournisseur:string;
    Responsable:string;
    Adresse:string;
    Ville:string;
    Tel:number;
    Email:string;
    Login:string;
    Mdp:string;
}

