export class Produit {
    IdP:number;
    IdProduit:string;
    Design:string;
    PU:number;
    TVA:number;
    Fodec:number;
    Strock_Init:string;
    Id_S_Famille:number;
    Id_Famille:number;
    Quantite:number;
    Montant:number;
}
