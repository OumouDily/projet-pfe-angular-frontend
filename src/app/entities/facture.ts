export class Facture {
    Id:number;
    Date_Facture:any;
    Fournisseur:number;
    Observation:string;
    TotHt:number;
    TotRem:number;
    TotTVA:number;
    TotTTC:number;
    Timbre:string;
    NET:number;
}
