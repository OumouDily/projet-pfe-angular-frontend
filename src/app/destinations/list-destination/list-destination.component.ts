import { Component, OnInit } from '@angular/core';
import { DestinationService } from 'src/app/services/destination.service';
import { ToastrService } from 'ngx-toastr';
import { Destination } from 'src/app/entities/destination';

@Component({
  selector: 'app-list-destination',
  templateUrl: './list-destination.component.html',
  styleUrls: ['./list-destination.component.css']
})
export class ListDestinationComponent implements OnInit {

  constructor(private destinationService:DestinationService,private toastr:ToastrService) { }

  ngOnInit() {
    this.destinationService.refreshList();
  }
  populateForm(f:Destination){
    this.destinationService.formData=Object.assign({},f);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.destinationService.DeleteDestination(id).subscribe(res=>{
      this.destinationService.refreshList();
      this.toastr.warning('Suppression effectuée!');
   
    })
  }
}
}
