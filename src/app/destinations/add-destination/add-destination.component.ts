import { Component, OnInit } from '@angular/core';
import { DestinationService } from 'src/app/services/destination.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-destination',
  templateUrl: './add-destination.component.html',
  styleUrls: ['./add-destination.component.css']
})
export class AddDestinationComponent implements OnInit {


 
 constructor(private destinationService:DestinationService,private toastr:ToastrService) { }

 ngOnInit() {
   this.resetForm();
  
   
 }
 resetForm(form? : NgForm){
   if(form!=null)
  form.resetForm();
  this.destinationService.formData={
    IdDes:null,
    LibelleDestination:'',
    Distance:0
  


   
  }
 }
 onSubmit(form: NgForm){
   if(form.value.IdDes==null)
   this.insertDestination(form);
   else
   this.updateDestination(form);
 }
 insertDestination(form: NgForm){
   this.destinationService.PostDestination(form.value).subscribe(res => {
   this.toastr.success('Insertion effectuée avec succès!');
  // alert("Insertion effectuée avec succès!");
   this.resetForm(form);
   this.destinationService.refreshList();
   });
 }
 updateDestination(form: NgForm){
   this.destinationService.PutDestination(form.value)
   .subscribe(res => {
     this.toastr.info('Modification effectuée!');
     //alert("Modification effectuée!");
     this.resetForm(form);
     this.destinationService.refreshList();
     });
 }
}
