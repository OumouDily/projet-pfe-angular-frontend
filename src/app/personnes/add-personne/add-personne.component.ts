import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';  
import { FormBuilder, FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { PersonneService } from 'src/app/services/personne.service';
import { Personne } from 'src/app/entities/personne';
import { GradeService } from 'src/app/services/grade.service';
import { FonctionService } from 'src/app/services/fonction.service';
import { ResidenceService } from 'src/app/services/residence.service';
import { Residence } from 'src/app/entities/residence';
import { Grade } from 'src/app/entities/grade';
import { Fonction } from 'src/app/entities/fonction';
@Component({
  selector: 'app-add-personne',
  templateUrl: './add-personne.component.html',
  styleUrls: ['./add-personne.component.css']
})
export class AddPersonneComponent implements OnInit {

  residenceList:Residence[];
  gradeList:Grade[];
  fonctionList:Fonction[];
  patternEmail='^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$';
  patternPhone='^((\\+91-?)|0)?[0-9]{10}$';
   constructor(private personneService:PersonneService,private residenceService:ResidenceService,private gradeService:GradeService,private fonctionService:FonctionService,private toastr:ToastrService) { }
 
   ngOnInit() {
     this.resetForm();
     this.residenceService.getResidenceList().then(res => this.residenceList = res as Residence[]);
     this.gradeService.getGradeList().then(res => this.gradeList = res as Grade[]);
     this.fonctionService.getFonctionList().then(res => this.fonctionList = res as Fonction[]);
    
    
   }
   resetForm(form? : NgForm){
     if(form!=null)
    form.resetForm();
    this.personneService.formData={
      IdP:null,
      Matricule:'',
    
     Nom:'',
     Prenom:'',
  
      Email:'',
      Tel:0,
      Adresse:'',
      Id_Grade:0,
      Id_Fonction:0,
      Id_Residence:0,
      NbreL:0
 
 
     
    }
   }
   onSubmit(form: NgForm){
     if(form.value.IdP==null)
     this.insertPersonne(form);
     else
     this.updatePersonne(form);
   }
   insertPersonne(form: NgForm){
     this.personneService.PostPersonne(form.value).subscribe(res => {
     this.toastr.success('Insertion effectuée avec succès!');
   
     this.resetForm(form);
     this.personneService.refreshList();
     });
   }
   updatePersonne(form: NgForm){
     this.personneService.PutPersonne(form.value)
     .subscribe(res => {
       this.toastr.info('Modification effectuée!');
      
       this.resetForm(form);
       this.personneService.refreshList();
       });
   }
}
