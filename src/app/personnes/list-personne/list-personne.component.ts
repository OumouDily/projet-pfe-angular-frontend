import { Component, OnInit } from '@angular/core';
import { PersonneService } from 'src/app/services/personne.service';
import { ToastrService } from 'ngx-toastr';
import { Personne } from 'src/app/entities/personne';

@Component({
  selector: 'app-list-personne',
  templateUrl: './list-personne.component.html',
  styleUrls: ['./list-personne.component.css']
})
export class ListPersonneComponent implements OnInit {

  constructor(private personneService:PersonneService,private toastr:ToastrService) { }

  ngOnInit() {
    this.personneService.refreshList();
  }
  populateForm(f:Personne){
    this.personneService.formData=Object.assign({},f);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.personneService.DeletePersonne(id).subscribe(res=>{
      this.personneService.refreshList();
      this.toastr.warning('Suppression effectuée!');
     
    })
  }
}

}
