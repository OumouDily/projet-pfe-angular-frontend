import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';  
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MarqueService } from 'src/app/services/marque.service';
import { Marque } from 'src/app/entities/marque';
import { ModelService } from 'src/app/services/model.service';
import { Model } from 'src/app/entities/model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-marque',
  templateUrl: './add-marque.component.html',
  styleUrls: ['./add-marque.component.css']
})
export class AddMarqueComponent implements OnInit {
  
  constructor(private marqueService:MarqueService,private toastr:ToastrService) { }

  ngOnInit() {
    this.resetForm();
    this.marqueService.getMarqueList();
  }
  resetForm(form? : NgForm){
    if(form!=null)
   form.resetForm();
   this.marqueService.formData={
     IdMa:null,
     LibelleMarque:''
    
   }
  }
  onSubmit(form: NgForm){
    if(form.value.IdMa==null)
    this.insertMarque(form);
    else
    this.updateMarque(form);
  }
  insertMarque(form: NgForm){
    this.marqueService.PostMarque(form.value).subscribe(res => {
   this.toastr.success('Insertion effectuée avec succès!');
    
    this.resetForm(form);
    this.marqueService.refreshList();
    });
  }
  updateMarque(form: NgForm){
    this.marqueService.PutMarque(form.value)
    .subscribe(res => {
     this.toastr.info('Modification effectuée!');
     
      this.resetForm(form);
      this.marqueService.refreshList();
      });
  }

}
