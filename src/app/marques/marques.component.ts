import { Component, OnInit } from '@angular/core';
import { MarqueService } from '../services/marque.service';

@Component({
  selector: 'app-marques',
  templateUrl: './marques.component.html',
  styleUrls: ['./marques.component.css']
})
export class MarquesComponent implements OnInit {

  constructor(private marqueService:MarqueService) { }

  ngOnInit() {
  }

}
