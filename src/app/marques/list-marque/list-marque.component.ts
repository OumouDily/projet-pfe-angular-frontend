import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MarqueService } from 'src/app/services/marque.service';
import { Marque } from 'src/app/entities/marque';

@Component({
  selector: 'app-list-marque',
  templateUrl: './list-marque.component.html',
  styleUrls: ['./list-marque.component.css']
})
export class ListMarqueComponent implements OnInit {

  constructor(private marqueService:MarqueService,private toastr:ToastrService) { }

  ngOnInit() {
    this.marqueService.refreshList();
  }
  populateForm(marq:Marque){
    this.marqueService.formData=Object.assign({},marq);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.marqueService.DeleteMarque(id).subscribe(res=>{
      this.marqueService.refreshList();
      this.toastr.warning('Suppression effectuée!');
     
    })
  }
}

}
