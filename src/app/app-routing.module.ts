
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



import { AddEnginComponent } from './engins/add-engin/add-engin.component';

import { ListEnginComponent } from './engins/list-engin/list-engin.component';


import { AddPersonneComponent } from './personnes/add-personne/add-personne.component';

import { ListPersonneComponent } from './personnes/list-personne/list-personne.component';
import { AddResidenceComponent } from './residences/add-residence/add-residence.component';

import { ListResidenceComponent } from './residences/list-residence/list-residence.component';

import { AddMarqueComponent } from './marques/add-marque/add-marque.component';

import { ListMarqueComponent } from './marques/list-marque/list-marque.component';
import { AddModelComponent } from './models/add-model/add-model.component';

import { ListModelComponent } from './models/list-model/list-model.component';
import { AddDirectionComponent } from './directions/add-direction/add-direction.component';

import { ListDirectionComponent } from './directions/list-direction/list-direction.component';

import { AddGradeComponent } from './grades/add-grade/add-grade.component';

import { ListGradeComponent } from './grades/list-grade/list-grade.component';
import { AddFonctionComponent } from './fonctions/add-fonction/add-fonction.component';

import { ListFonctionComponent } from './fonctions/list-fonction/list-fonction.component';




import { AddCarburantComponent } from './carburants/add-carburant/add-carburant.component';


import { AddSortieComponent } from './sorties/add-sortie/add-sortie.component';


import { AddEntreeComponent } from './entrees/add-entree/add-entree.component';


import { CarburantsComponent } from './carburants/carburants.component';



import { AddFamilleComponent } from './familles/add-famille/add-famille.component';
import { ListFamilleComponent } from './familles/list-famille/list-famille.component';
import { ReparationsComponent } from './reparations/reparations.component';

import { ListTypeComponent } from './types/list-type/list-type.component';
import { AddTypeComponent } from './types/add-type/add-type.component';



import { MenuComponent } from './menu/menu.component';


import { ReparationExterneComponent } from './reparation-externe/reparation-externe.component';


import { AddFactureComponent } from './Factures/add-facture/add-facture.component';
import { ListFactureComponent } from './Factures/list-facture/list-facture.component';

import { AddFournisseurComponent } from './Fournisseurs/add-fournisseur/add-fournisseur.component';
import { ListFournisseurComponent } from './Fournisseurs/list-fournisseur/list-fournisseur.component';

import { AddDestinationComponent } from './destinations/add-destination/add-destination.component';
import { ListDestinationComponent } from './destinations/list-destination/list-destination.component';

import { AddMissionComponent } from './missions/add-mission/add-mission.component';
import { ListMissionComponent } from './missions/list-mission/list-mission.component';


import { AddDemandeComponent } from './demande/add-demande/add-demande.component';
import { ListDemandeComponent } from './demande/list-demande/list-demande.component';
import { LoginComponent } from './login/login.component';

import { RegisterComponent } from './register/register.component';
import { AddMotifComponent } from './motifs/add-motif/add-motif.component';
import { ListMotifComponent } from './motifs/list-motif/list-motif.component';

import { ListepdfMissionComponent } from './missions/listepdf-mission/listepdf-mission.component';
import { ListepdfDemandeComponent } from './demande/listepdf-demande/listepdf-demande.component';
import { ListepdfFactureComponent } from './factures/listepdf-facture/listepdf-facture.component';
import { ListepdfEnginComponent } from './engins/listepdf-engin/listepdf-engin.component';

import { AddSfamilleComponent } from './Sfamilles/add-sfamille/add-sfamille.component';
import { ListSfamilleComponent } from './Sfamilles/list-sfamille/list-sfamille.component';
import { ReparationListeComponent } from './reparations/reparation-liste/reparation-liste.component';
import { ReparationElisteComponent } from './reparation-externe/reparation-eliste/reparation-eliste.component';

import { AddUserComponent } from './register/add-user/add-user.component';
import { ListUserComponent } from './register/list-user/list-user.component';

import { AddEntretienComponent } from './entretiens/add-entretien/add-entretien.component';
import { ListEntretienComponent } from './entretiens/list-entretien/list-entretien.component';
import { ListepdfEntretienComponent } from './entretiens/listepdf-entretien/listepdf-entretien.component';

import { EnginParcComponent } from './engins/engin-parc/engin-parc.component';
import { AddProduitComponent } from './produits/add-produit/add-produit.component';
import { ListProduitComponent } from './produits/list-produit/list-produit.component';

import { ListEntreeComponent } from './entrees/list-entree/list-entree.component';
import { ListSortieComponent } from './sorties/list-sortie/list-sortie.component';
import { AuthGuardService } from './services/auth-guard.service';

import { EntreesComponent } from './entrees/entrees.component';
import { SortiesComponent } from './sorties/sorties.component';
import { ListePdfEntreeComponent } from './entrees/liste-pdf-entree/liste-pdf-entree.component';
import { ListePdfSortieComponent } from './sorties/liste-pdf-sortie/liste-pdf-sortie.component';
import { AddAffectationComponent } from './affectations/add-affectation/add-affectation.component';
import { ListAffectationComponent } from './affectations/list-affectation/list-affectation.component';
import { AddConsommationComponent } from './consommations/add-consommation/add-consommation.component';
import { ListConsommationComponent } from './consommations/list-consommation/list-consommation.component';
import { EnginReserveComponent } from './engins/engin-reserve/engin-reserve.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ModelsComponent } from './models/models.component';
import { MarquesComponent } from './marques/marques.component';
import { TypesComponent } from './types/types.component';
import { MotifsComponent } from './motifs/motifs.component';
import { FamillesComponent } from './familles/familles.component';
import { SfamillesComponent } from './sfamilles/sfamilles.component';
import { GradesComponent } from './grades/grades.component';
import { FonctionsComponent } from './fonctions/fonctions.component';
import { DirectionsComponent } from './directions/directions.component';
import { ResidencesComponent } from './residences/residences.component';
import { DestinationsComponent } from './destinations/destinations.component';
import { UploadComponent } from './upload/upload.component';
import { ListepdfConsommationComponent } from './consommations/listepdf-consommation/listepdf-consommation.component';
import { ListPdfFournisseurComponent } from './fournisseurs/list-pdf-fournisseur/list-pdf-fournisseur.component';
import { Dashboard1Component } from './dashboard1/dashboard1.component';
import { LoginAdminComponent } from './login-admin/login-admin.component';




  const appRoutes:Routes=[
    {path:'',component:MenuComponent,canActivate:[AuthGuardService],children:[
  
    {path:'TableauDeBord',component:Dashboard1Component},
    {path:'dashboard',component:DashboardComponent},
    {path:'add-engin',component:AddEnginComponent},
    {path:'list-engin',component:ListEnginComponent},
    {path:'ListePDFEngin',component:ListepdfEnginComponent},
    {path:'engin-parc',component:EnginParcComponent},
    {path:'engin-reserve',component:EnginReserveComponent},
    {path:'affectation',component:AddAffectationComponent},
    {path:'list-affectation',component:ListAffectationComponent},
    {path:'add-consommation',component:AddConsommationComponent},
    {path:'list-consommation',component:ListConsommationComponent},
    {path:'ListePDFConsommation',component:ListepdfConsommationComponent},
    {path:'add-residence',component:AddResidenceComponent},
    {path:'list-residence',component:ResidencesComponent},
    {path:'add-marque',component:AddMarqueComponent},
    {path:'list-marque',component:MarquesComponent},
    {path:'add-carburant',component:AddCarburantComponent},
    {path:'list-carburant',component:CarburantsComponent},
    {path:'add-famille',component:AddFamilleComponent},
    {path:'list-famille',component:FamillesComponent},
    {path:'add-model',component:AddModelComponent},
    {path:'list-model',component:ModelsComponent},
    {path:'add-type',component:AddTypeComponent},
    {path:'list-type',component:TypesComponent},
    {path:'DemandeMission',component:AddDemandeComponent},
    {path:'ListeDemandeMission',component:ListDemandeComponent},
    {path:'ListePDFDemandeMission',component:ListepdfDemandeComponent},
    {path:'Mission',component:AddMissionComponent},
    {path:'ListeMission',component:ListMissionComponent},
    {path:'ListePDFMission',component:ListepdfMissionComponent},
    {path:'Reparations',component:ReparationsComponent},
    {path:'ReparationListe',component:ReparationListeComponent},
    {path:'reparation/modification/:id',component:ReparationsComponent},
    {path:'reparations',component:ReparationExterneComponent},
    {path:'ReparationEListe',component:ReparationElisteComponent},
    {path:'reparationE/modification/:id',component:ReparationExterneComponent},
    {path:'entree',component:AddEntreeComponent},
    {path:'list-entree',component:EntreesComponent},
    {path:'pdfEntree',component:ListePdfEntreeComponent},
    {path:'sortie',component:AddSortieComponent},
    {path:'list-sortie',component:SortiesComponent},
    {path:'pdfSortie',component:ListePdfSortieComponent},
    {path:'app-menu',component:MenuComponent},

    {path:'add-facture',component:AddFactureComponent},
    {path:'list-facture',component:ListFactureComponent},
    {path:'ListePDFFacture',component:ListepdfFactureComponent},
   
    {path:'add-entretien',component:AddEntretienComponent},
    {path:'list-entretien',component:ListEntretienComponent},
    {path:'ListePDFEntretien',component:ListepdfEntretienComponent},
    {path:'add-fournisseur',component:AddFournisseurComponent},
    {path:'list-fournisseur',component:ListFournisseurComponent},
    {path:'ListePDFFournisseur',component:ListPdfFournisseurComponent},
    {path:'add-personne',component:AddPersonneComponent},
    {path:'list-personne',component:ListPersonneComponent},
    {path:'add-grade',component:AddGradeComponent},
    {path:'list-grade',component:GradesComponent},
    {path:'add-fonction',component:AddFonctionComponent},
    {path:'list-fonction',component:FonctionsComponent},
    {path:'add-direction',component:AddDirectionComponent},
    {path:'list-direction',component:DirectionsComponent},
    {path:'add-destination',component:AddDestinationComponent},
    {path:'list-destination',component:DestinationsComponent},
    {path:'add-motif',component:AddMotifComponent},
    {path:'list-motif',component:MotifsComponent},
    {path:'add-sfamille',component:AddSfamilleComponent},
    {path:'list-sfamille',component:SfamillesComponent},
    {path:'add-user',component:AddUserComponent},
    {path:'list-user',component:ListUserComponent},
    {path:'add-produit',component:AddProduitComponent},
    {path:'upload',component:UploadComponent},
    {path:'list-produit',component:ListProduitComponent}]},

    {path:'Login',component:LoginComponent},
    {path:'LoginAdmin',component:LoginAdminComponent},
    {path:'Register',component:RegisterComponent}
  
  ]

  @NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }
  