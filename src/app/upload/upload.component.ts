import { Component, OnInit } from '@angular/core';

import { HttpResponse, HttpEventType } from '@angular/common/http';
import { UploadService } from '../services/upload.service';
@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

    selectedFiles: FileList;
    currentFileUpload: File;
    
   
    constructor(private uploadService: UploadService) { }
   
    ngOnInit() {
    }
   
    selectFile(event) {
      this.selectedFiles = event.target.files;
    }
   
    upload() {

   
      this.currentFileUpload = this.selectedFiles.item(0);
      this.uploadService.pushFileToStorage(this.currentFileUpload , localStorage.getItem('id'))
      .subscribe(event => {

      
        
     
        if (event.type === HttpEventType.UploadProgress) {
         
        } else if (event instanceof HttpResponse) {
          const body= event.body;


  
    const str =   JSON.stringify(body);
    const result = str.substring(16,str.length-4);
    
   
        localStorage.setItem('pic', result);
     location.reload();
      
          console.log('File is completely uploaded!');

        }
      });
   
      this.selectedFiles = undefined;
    }

}
