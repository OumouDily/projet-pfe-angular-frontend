import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';  
import { FormBuilder, FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { ResidenceService } from 'src/app/services/residence.service';
import { Residence } from 'src/app/entities/residence';
import { Direction } from 'src/app/entities/direction';
import { DirectionService } from 'src/app/services/direction.service';
import { Model } from 'src/app/entities/model';

@Component({
  selector: 'app-add-residence',
  templateUrl: './add-residence.component.html',
  styleUrls: ['./add-residence.component.css']
})
export class AddResidenceComponent implements OnInit {
  
  modelList:Model;
 
 directionList:Direction[];
 constructor(private residenceService:ResidenceService,private directionService:DirectionService,private toastr:ToastrService) { }

 ngOnInit() {
   this.resetForm();
   this.directionService.getDirectionList().then(res => this.directionList = res as Direction[]);
 }
 resetForm(form? : NgForm){
   if(form!=null)
  form.resetForm();
 this.residenceService.formData={
    IdR:null,
    LibelleResidence:'',
    Id_Direction:0
   
  }
 }
 onSubmit(form: NgForm){
   if(form.value.IdR==null)
   this.insertResidence(form);
   else
   this.updateResidence(form);
 }
 insertResidence(form: NgForm){
   this.residenceService.PostResidence(form.value).subscribe(res => {
   this.toastr.success('Insertion effectuée avec succès!');
  
   this.resetForm(form);
   this.residenceService.refreshList();
   });
 }
 updateResidence(form: NgForm){
   this.residenceService.PutResidence(form.value)
   .subscribe(res => {
     this.toastr.info('Modification effectuée!');
    
     this.resetForm(form);
     this.residenceService.refreshList();
     });
 }
}
