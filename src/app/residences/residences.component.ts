import { Component, OnInit } from '@angular/core';
import { ResidenceService } from '../services/residence.service';

@Component({
  selector: 'app-residences',
  templateUrl: './residences.component.html',
  styleUrls: ['./residences.component.css']
})
export class ResidencesComponent implements OnInit {

  constructor(private residenceService:ResidenceService) { }

  ngOnInit() {
  }

}
