import { Component, OnInit } from '@angular/core';
import { ResidenceService } from 'src/app/services/residence.service';
import { ToastrService } from 'ngx-toastr';
import { Residence } from 'src/app/entities/residence';

@Component({
  selector: 'app-list-residence',
  templateUrl: './list-residence.component.html',
  styleUrls: ['./list-residence.component.css']
})
export class ListResidenceComponent implements OnInit {
  
 
  constructor(private residenceService:ResidenceService,private toastr:ToastrService) { }

  ngOnInit() {
   this.residenceService.refreshList();
 }
  populateForm(r:Residence){
    this.residenceService.formData=Object.assign({},r);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.residenceService.DeleteResidence(id).subscribe(res=>{
      this.residenceService.refreshList();
      this.toastr.warning('Suppression effectuée!');
     
    })
  }
}

}
