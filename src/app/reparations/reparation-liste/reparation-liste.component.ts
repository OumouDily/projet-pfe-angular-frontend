import { Component, OnInit } from '@angular/core';
import { ReparationService } from 'src/app/services/reparation.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Reparation } from 'src/app/entities/reparation';

@Component({
  selector: 'app-reparation-liste',
  templateUrl: './reparation-liste.component.html',
  styleUrls: ['./reparation-liste.component.css']
})
export class ReparationListeComponent implements OnInit {
  reparationList;
  constructor(private service:ReparationService,private router:Router, private toastr: ToastrService) { }

  ngOnInit() {
    this.refreshList();
  }
  refreshList(){
    this.service.getReparationList().then(res => this.reparationList = res);
  }
  openForEdit(Id:number){
this.router.navigate(['/reparation/modification/'+Id]);
  }
  onReparationDelete(id:number){
    if(confirm('Etes-vous sur de vouloir supprimer?')){
this.service.deleteReparation(id).then(res=>{
  this.refreshList();
  this.toastr.warning('Suppression effectuée avec succès!');
});
  }
}

selectReparation(f :Reparation){
  
  this.service.formData = f;
 
  this.router.navigate(['/Reparations']);
}
}
