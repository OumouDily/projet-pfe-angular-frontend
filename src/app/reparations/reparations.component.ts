import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ReparationService } from '../services/reparation.service';
import { LReparationComponent } from '../lreparation/lreparation.component';
import { EnginService } from '../services/engin.service';
import { Engin } from '../entities/engin';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-reparations',
  templateUrl: './reparations.component.html',
  styleUrls: ['./reparations.component.css']
})
export class ReparationsComponent implements OnInit {
 
  minDate  ;
  enginList:Engin[];
  isValid:boolean=true;

  constructor(private service:ReparationService,
    private enginService:EnginService,
    private dialog:MatDialog,
    private router:Router,
    private toastr: ToastrService,
    private currentRoute:ActivatedRoute,private datePipe: DatePipe) { }

  ngOnInit() {
    
    this.minDate = this.transformDate(new Date());
    let Id=this.currentRoute.snapshot.paramMap.get('id');
    if(Id==null)
    this.resetForm();
    else{
      this.service.getReparationByID(parseInt(Id)).then(res=>{
      this.service.formData=res.reparation;
      this.service.LreparationList=res.reparationDetails;
      });
    }
    this.enginService.getEnginList().then(res => this.enginList = res as Engin[]);
   
  }
  transformDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }
  resetForm(form?:NgForm){
    if(form = null)
    form.resetForm();
    this.service.formData={
      IdR:null,
      Numero:Math.floor(100000+Math.random()*900000).toString(),
      Id_Engin:0,
      Observation:'',
      Total:0,
      Date:'',
      DeletedReparationId:''
    };
   this.service.LreparationList=[];
  }

  AddOrEdit(orderItemIndex,IdR){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width="50%";
    dialogConfig.data={orderItemIndex,IdR};

    this.dialog.open(LReparationComponent, dialogConfig).afterClosed().subscribe(res=>{
      this.updateGrandTotal();
    });

  }
  onDelete(Id:number,i:number){
    if(Id!=null)
    this.service.formData.DeletedReparationId+=Id+",";
   this.service.LreparationList.splice(i,1); 
   this.updateGrandTotal();
  }

  updateGrandTotal(){
    this.service.formData.Total=this.service.LreparationList.reduce((prev,curr)=>{
      return prev+curr.Montant;
    },0);
    this.service.formData.Total=parseFloat(this.service.formData.Total.toFixed(2));
  }
  validateForm(){
    this.isValid=true;
    if(this.service.formData.Id_Engin==0)
    this.isValid=false;
    else if(this.service.LreparationList.length==0)
    this.isValid=false;
    return this.isValid;
  }
  onSubmit(form:NgForm){
   if(this.validateForm()){
    this.service.saveOrUpdate().subscribe(res=>{
  this.resetForm();
  this.toastr.success('Insertion effectuée avec succès!');
 
  this.router.navigate(['/Reparations']);
    })
      
   }
  }
}
