import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login-admin',
  templateUrl: './login-admin.component.html',
  styleUrls: ['./login-admin.component.css']
})
export class LoginAdminComponent implements OnInit {

 
  error='';
  rootURL: string;

  User: any = {};
  constructor(private   userService : UserService,private router: Router, 
    private authenticationService: AuthService) { }




  ngOnInit() {
    this.userService.isLogin = false;
  }


  authentifier() {
  
    this.authenticationService.login(this.User).subscribe( res => {
      localStorage.setItem('id', res.Id);
      localStorage.setItem('pic', res.Picture);
      localStorage.setItem('name', res.Nom);
      this.authenticationService.storeToekn(res.Token);
      this.router.navigate(['/dashboard']);
      

    }, ex => {
      this.error ="Erreur d'authentification: Merci de verifier votre login ou mot de passe"
      console.log(ex);
    });
  }

}
