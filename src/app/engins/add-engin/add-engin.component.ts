import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';  
import { FormBuilder, FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { EnginService } from 'src/app/services/engin.service';
import { Engin } from 'src/app/entities/engin';
import { Marque } from 'src/app/entities/marque';
import { TypeEngin } from 'src/app/entities/typeEngin';
import { Direction } from 'src/app/entities/direction';
import { Model } from 'src/app/entities/model';
import { ModelService } from 'src/app/services/model.service';
import { MarqueService } from 'src/app/services/marque.service';
import { DirectionService } from 'src/app/services/direction.service';
import { TypeEnginsService } from 'src/app/services/type-engins.service';
import { Entree } from 'src/app/entities/entree';
import { EntreeService } from 'src/app/services/entree.service';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-add-engin',
  templateUrl: './add-engin.component.html',
  styleUrls: ['./add-engin.component.css']
})
export class AddEnginComponent implements OnInit {

 
  marqueList:Marque[];
  typeList:TypeEngin[];
  directionList:Direction[];
  modelList:Model[];
  entreeList:Entree[];
  minDate  ;
  
  constructor(private entreeService:EntreeService,private modelService:ModelService,
    private directionService:DirectionService,private enginService:EnginService,
    private typeService:TypeEnginsService,private marqueService:MarqueService,
    private toastr:ToastrService,private datePipe: DatePipe) { }

  ngOnInit() {
   this.minDate = this.transformDate(new Date());
    this.resetForm();
    this.marqueService.getMarqueList().then(res => this.marqueList = res as Marque[]);
    this.modelService.getModelList().then(res => this.modelList = res as Model[]);
    this.typeService.getTypeList().then(res => this.typeList = res as TypeEngin[]);
    this.entreeService.getEntreeList().then(res => this.entreeList = res as Entree[]);
    this.directionService.getDirectionList().then(res => this.directionList = res as Direction[]);


  }

  transformDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }
  resetForm(form? : NgForm){
    if(form!=null)
   form.resetForm();
   this.enginService.formData={
     IdE:null,
     Matricule:'',
    Date_Acquis:'',
     Val_Acquis:0,
    Date_Mise_Service:'',
    Nature:'',
    NSerie:0,
    Date_Visite_Technique:'',
     Date_Reparation:'',
     PU:0,
     Nbre_Place:0,
     Mat_Origine:'',
     Id_Type:0,
     Id_Model:0,
     Id_Marque:0,
     Id_Direction:0,
     numeroEntree:0,
     numeroSortie:0


    
   }
  }
  onSubmit(form: NgForm){
    if(form.value.IdE==null)
    this.insertEngin(form);
    else
    this.updateEngin(form);
  }
  insertEngin(form: NgForm){
    this.enginService.PostEngin(form.value).subscribe(res => {
    this.toastr.success('Insertion effectuée avec succès!');
  
    this.resetForm(form);
    this.enginService.refreshList();
    });
  }
  updateEngin(form: NgForm){
    this.enginService.PutEngin(form.value)
    .subscribe(res => {
      this.toastr.info('Modification effectuée!');
    
      this.resetForm(form);
      this.enginService.refreshList();
      });
  }

}
