import { Component, OnInit, Inject} from '@angular/core';
import { EnginService } from 'src/app/services/engin.service';
import { ToastrService } from 'ngx-toastr';
import { Engin } from 'src/app/entities/engin';



@Component({
  selector: 'app-list-engin',
  templateUrl: './list-engin.component.html',
  styleUrls: ['./list-engin.component.css'],
  providers: [
    { provide: 'Window',  useValue: window }
  ]
})
export class ListEnginComponent implements OnInit {
  
 

 p:number=1;
  constructor(private enginService:EnginService,private toastr:ToastrService) { }

  ngOnInit():void {
    this.enginService.refreshList();
  
  }
 

  populateForm(f:Engin){
    this.enginService.formData=Object.assign({},f);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.enginService.DeleteEngin(id).subscribe(res=>{
      this.enginService.refreshList();
      this.toastr.warning('Suppression effectuée!');
      
    })
  }
}

}
