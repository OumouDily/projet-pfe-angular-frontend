import { Component, OnInit } from '@angular/core';
import { EnginService } from 'src/app/services/engin.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Engin } from 'src/app/entities/engin';

@Component({
  selector: 'app-engin-reserve',
  templateUrl: './engin-reserve.component.html',
  styleUrls: ['./engin-reserve.component.css']
})
export class EnginReserveComponent implements OnInit {

  constructor(private enginService:EnginService,private toastr:ToastrService,private currentRoute:ActivatedRoute) { }

  ngOnInit() {
    let Id=this.currentRoute.snapshot.paramMap.get('id');
    if(Id==null)
    this.resetForm();
    else{
      this.enginService.getEnginByID(parseInt(Id)).then(res=>{
      this.enginService.enginList=res.engin;
      
      });
    this.enginService.refreshList();
  }
}
resetForm(form?:NgForm){
  if(form = null)
  form.resetForm();
  this.enginService.formData={
    IdE:null,
    Matricule:'',
    Date_Acquis:'',
    Val_Acquis:0,
    Date_Mise_Service:'',
    Nature:'',
    NSerie:0,
    Date_Visite_Technique:'',
    Date_Reparation:'',
    PU:0,
    Nbre_Place:0,
    Mat_Origine:'',
    Id_Type:0,
    Id_Model:0,
    Id_Marque:0,
    Id_Direction:0,
    numeroEntree:0,
    numeroSortie:0
  };
 
}
  populateForm(f:Engin){
    this.enginService.formData=Object.assign({},f);
  }

}
