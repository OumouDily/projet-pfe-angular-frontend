import { Component, OnInit } from '@angular/core';
import { EnginService } from 'src/app/services/engin.service';
import { ToastrService } from 'ngx-toastr';
import { Engin } from 'src/app/entities/engin';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
@Component({
  selector: 'app-listepdf-engin',
  templateUrl: './listepdf-engin.component.html',
  styleUrls: ['./listepdf-engin.component.css']
})
export class ListepdfEnginComponent implements OnInit {
  constructor(private enginService:EnginService,private toastr:ToastrService) { }

  ngOnInit():void {
    this.enginService.refreshList();
  
  }
 

  populateForm(f:Engin){
    this.enginService.formData=Object.assign({},f);
  }
  generatePdf() {
 
    const div = document.getElementById("Engin");
    const options = {background: "white", height: div.clientHeight, width: div.clientWidth};
  
    html2canvas(div, options).then((canvas) => {
  
        
        //Initialize JSPDF
        let doc = new jsPDF("p", "mm", "a4");
     
        //Converting canvas to Image
        let imgData = canvas.toDataURL("image/PNG");
  
        
        //Add image Canvas to PDF
        doc.addImage(imgData, 'PNG',5,0,190,100);
  
        let pdfOutput = doc.output();
        // using ArrayBuffer will allow you to put image inside PDF
        let buffer = new ArrayBuffer(pdfOutput.length);
        let array = new Uint8Array(buffer);
        for (let i = 0; i < pdfOutput.length; i++) {
            array[i] = pdfOutput.charCodeAt(i);
        }
  
        //Name of pdf
        const fileName = "ListeEngin.pdf";
  
        // Make file
        doc.save(fileName);
  
    });
  }
}
