import { Component, OnInit } from '@angular/core';
import { CarburantService } from 'src/app/services/carburant.service';
import { Carburant } from 'src/app/entities/carburant';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-carburant',
  templateUrl: './list-carburant.component.html',
  styleUrls: ['./list-carburant.component.css']
})
export class ListCarburantComponent implements OnInit {

  constructor(private carburantService:CarburantService,private toastr:ToastrService) { }

  ngOnInit() {
    this.carburantService.refreshList();
  }
  populateForm(c:Carburant){
    this.carburantService.formData=Object.assign({},c);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.carburantService.DeleteCarburant(id).subscribe(res=>{
      this.carburantService.refreshList();
      this.toastr.warning('Suppression effectuée!');
     
    })
  }
}
}
