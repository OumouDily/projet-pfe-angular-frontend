import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';  
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { CarburantService } from 'src/app/services/carburant.service';
import { Carburant } from 'src/app/entities/carburant';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-carburant',
  templateUrl: './add-carburant.component.html',
  styleUrls: ['./add-carburant.component.css']
})
export class AddCarburantComponent implements OnInit {

  constructor(private carburantService:CarburantService,private toastr:ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }
  resetForm(form? : NgForm){
    if(form!=null)
   form.resetForm();
   this.carburantService.formData={
     IdC:null,
     LibelleCarburant:''
    
   }
  }
  onSubmit(form: NgForm){
    if(form.value.IdC==null)
    this.insertCarburant(form);
    else
    this.updateCarburant(form);
  }
  insertCarburant(form: NgForm){
    this.carburantService.PostCarburant(form.value).subscribe(res => {
    this.toastr.success('Insertion effectuée avec succès!');
   
    this.resetForm(form);
    this.carburantService.refreshList();
    });
  }
  updateCarburant(form: NgForm){
    this.carburantService.PutCarburant(form.value)
    .subscribe(res => {
      this.toastr.info('Modification effectuée!');
     
      this.resetForm(form);
      this.carburantService.refreshList();
      });
  }
}
