import { Component, OnInit } from '@angular/core';
import { CarburantService } from '../services/carburant.service';

@Component({
  selector: 'app-carburants',
  templateUrl: './carburants.component.html',
  styleUrls: ['./carburants.component.css']
})
export class CarburantsComponent implements OnInit {

  constructor(private carburantService:CarburantService) { }

  ngOnInit() {
  }

}
