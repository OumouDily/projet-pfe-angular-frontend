import { Component, OnInit } from '@angular/core';
import { EnginService } from 'src/app/services/engin.service';
import { ToastrService } from 'ngx-toastr';

import { NgForm } from '@angular/forms';
import { DemandeMission } from 'src/app/entities/demande-mission';
import { Destination } from 'src/app/entities/destination';
import { Engin } from 'src/app/entities/engin';
import { MissionService } from 'src/app/services/mission.service';
import { DestinationService } from 'src/app/services/destination.service';
import { DemandeService } from 'src/app/services/demande.service';

@Component({
  selector: 'app-add-mission',
  templateUrl: './add-mission.component.html',
  styleUrls: ['./add-mission.component.css']
})
export class AddMissionComponent implements OnInit {


 demandeList:DemandeMission[];
 enginList:Engin[];
 destinationList:Destination[];
 
 constructor(private missionService:MissionService,private demandeService:DemandeService,private enginService:EnginService,private destinationService:DestinationService,private toastr:ToastrService) { }

 ngOnInit() {
   this.resetForm();
   this.demandeService.getDemandeList().then(res => this.demandeList = res as DemandeMission[]);
   this.enginService.getEnginList().then(res => this.enginList = res as Engin[]);
   this.destinationService.getDestinationList().then(res => this.destinationList = res as Destination[]);
   
 }
 resetForm(form? : NgForm){
   if(form!=null)
  form.resetForm();
  this.missionService.formData={
    IdMi:null,
    Numero:Math.floor(100000+Math.random()*900000).toString(),
    Date_Mission:'',
    Objet:'',
   NumeroDemande:0,
   Destination:0,
   IdEngin:0,
   Date_Debut:'',
   Date_Fin:'',
   Personne:'',
   NbreL:0
  


   
  }
 }
 onSubmit(form: NgForm){
   if(form.value.IdMi==null)
   this.insertMission(form);
   else
   this.updateMission(form);
 }
 insertMission(form: NgForm){
   this.missionService.PostMission(form.value).subscribe(res => {
   this.toastr.success('Insertion effectuée avec succès!');
  
   this.resetForm(form);
   this.missionService.refreshList();
   });
 }
 updateMission(form: NgForm){
   this.missionService.PutMission(form.value)
   .subscribe(res => {
     this.toastr.info('Modification effectuée!');
     
     this.resetForm(form);
     this.missionService.refreshList();
     });
 }

}
