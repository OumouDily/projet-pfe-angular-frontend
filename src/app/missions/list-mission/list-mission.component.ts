import { Component, OnInit, ViewChild } from '@angular/core';
import { MissionService } from 'src/app/services/mission.service';
import { ToastrService } from 'ngx-toastr';
import { Mission } from 'src/app/entities/mission';
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';

@Component({
  selector: 'app-list-mission',
  templateUrl: './list-mission.component.html',
  styleUrls: ['./list-mission.component.css']
})

export class ListMissionComponent implements OnInit {

 

  constructor(private missionService:MissionService,private toastr:ToastrService) { }
  
  ngOnInit() {
    this.missionService.refreshList();

    

  }
 
  

  populateForm(f:Mission){
    this.missionService.formData=Object.assign({},f);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.missionService.DeleteMission(id).subscribe(res=>{
      this.missionService.refreshList();
      this.toastr.warning('Suppression effectuée!');
    
    })
  }
}




}
