import { Component, OnInit } from '@angular/core';
import { MissionService } from 'src/app/services/mission.service';
import { ToastrService } from 'ngx-toastr';
import { Mission } from 'src/app/entities/mission';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-listepdf-mission',
  templateUrl: './listepdf-mission.component.html',
  styleUrls: ['./listepdf-mission.component.css']
})
export class ListepdfMissionComponent implements OnInit {

  constructor(private missionService:MissionService,private toastr:ToastrService) { }

  ngOnInit() {
    this.missionService.refreshList();
  }
  populateForm(f:Mission){
    this.missionService.formData=Object.assign({},f);
  }

 generatePdf() {
 
    const div = document.getElementById("Mission");
    const options = {background: "white", height: div.clientHeight, width: div.clientWidth};
  
    html2canvas(div, options).then((canvas) => {
  
        
        //Initialize JSPDF
        let doc = new jsPDF("p", "mm", "a4");
     
        //Converting canvas to Image
        let imgData = canvas.toDataURL("image/PNG");
  
        
        //Add image Canvas to PDF
        doc.addImage(imgData, 'PNG',5,0,190,100);
  
        let pdfOutput = doc.output();
        // using ArrayBuffer will allow you to put image inside PDF
        let buffer = new ArrayBuffer(pdfOutput.length);
        let array = new Uint8Array(buffer);
        for (let i = 0; i < pdfOutput.length; i++) {
            array[i] = pdfOutput.charCodeAt(i);
        }
  
        //Name of pdf
        const fileName = "ListeMission.pdf";
  
        // Make file
        doc.save(fileName);
  
    });
  }

}
