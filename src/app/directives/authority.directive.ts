import { Directive } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appAuthority]'
})
export class AuthorityDirective {

  private authorities: string[];
  constructor(private authenticationService: AuthService, private templateRef: TemplateRef<any>, private viewContainerRef: ViewContainerRef) {}

  @Input()
  set appAuthority(value: string | string[]) {
    this.authorities = typeof value === 'string' ? [value] : value;
    this.updateView();
  }

  private updateView(): void {
    const hasAnyAuthority = this.authenticationService.hasAnyAuthority(this.authorities);
    this.viewContainerRef.clear();
    if (hasAnyAuthority) {
      this.viewContainerRef.createEmbeddedView(this.templateRef);
    }
  }

}
