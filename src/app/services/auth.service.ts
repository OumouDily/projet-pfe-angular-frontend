import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';



@Injectable({
  providedIn: 'root'
})
export class AuthService {


  private BASE_URL= 'http://localhost:63003/api';
  constructor(private httpClient: HttpClient) { }

  public login(User: any): Observable<any> {
  return  this.httpClient.post(this.BASE_URL + '/auth', User );
  }


  public storeToekn(token) {
    sessionStorage.setItem('token', token);
  }


  public logout() {
    sessionStorage.removeItem('token');
    location.reload();
  }


  hasAnyAuthority(authorities) : boolean {
    const jwtHelperService = new JwtHelperService();
    const decodedToken = jwtHelperService.decodeToken(sessionStorage.getItem('token'));
    const role = decodedToken.role;
    let valid = false;
    console.log(authorities);
    authorities.forEach(element => {

      if(element === role) {
        valid = true;
      }
      
    });
console.log(valid);

    return valid;
  }

}
