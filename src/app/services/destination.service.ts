import { Injectable } from '@angular/core';
import { Destination } from '../entities/destination';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DestinationService {

  formData:Destination;
 
  destinationList:Destination[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient,private destinationService:DestinationService) { }
  PostDestination(formData:Destination){
   return this.http.post(this.rootURL+'/Destinations',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Destinations').toPromise().then(res => this.destinationList= res as Destination[]);

  }
  PutDestination(formData:Destination){
    return this.http.put(this.rootURL+'/Destinations/'+formData.IdDes,formData);
   }
   DeleteDestination(id:number)
   {
    return this.http.delete(this.rootURL+'/Destinations/'+id);
   }
   getDestinationList(){
    return this.http.get(environment.apiURL+'/Destinations').toPromise();
  }
}
