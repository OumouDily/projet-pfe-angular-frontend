import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Direction } from '../entities/direction';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DirectionService {

  formData:Direction;
  list:Direction[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient) { }
  PostDirection(formData:Direction){
   return this.http.post(this.rootURL+'/Directions',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Directions').toPromise().then(res => this.list= res as Direction[]);

  }
  PutDirection(formData:Direction){
    return this.http.put(this.rootURL+'/Directions/'+formData.IdD,formData);
   }
   DeleteDirection(id:number)
   {
    return this.http.delete(this.rootURL+'/Directions/'+id);
   }
   getDirectionList(){
    return this.http.get(environment.apiURL+'/Directions').toPromise();
  }

}
