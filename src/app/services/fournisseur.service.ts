import { Injectable } from '@angular/core';
import { Fournisseur } from '../entities/fournisseur';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class FournisseurService {

  formData:Fournisseur;
  listFournisseur:Fournisseur[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient) { }
  PostFournisseur(formData:Fournisseur){
   return this.http.post(this.rootURL+'/Fournisseurs',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Fournisseurs').toPromise().then(res => this.listFournisseur= res as Fournisseur[]);

  }
  PutFournisseur(formData:Fournisseur){
    return this.http.put(this.rootURL+'/Fournisseurs/'+formData.IdFo,formData);
   }
   DeleteFournisseur(id:number)
   {
    return this.http.delete(this.rootURL+'/Fournisseurs/'+id);
   }
   getFournisseurList(){
    return this.http.get(environment.apiURL+'/Fournisseurs').toPromise();
  }
}
