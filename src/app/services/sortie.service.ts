import { Injectable } from '@angular/core';
import { Sortie } from '../entities/sortie';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SortieService {

  formData:Sortie;
 
  sortieList:Sortie[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient,private sortieService:SortieService) { }
  PostSortie(formData:Sortie){
   return this.http.post(this.rootURL+'/Sorties',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Sorties').toPromise().then(res => this.sortieList= res as Sortie[]);

  }
  PutSortie(formData:Sortie){
    return this.http.put(this.rootURL+'/Sorties/'+formData.Id,formData);
   }
   DeleteSortie(id:number)
   {
    return this.http.delete(this.rootURL+'/Sorties/'+id);
   }
   getSortieList(){
    return this.http.get(environment.apiURL+'/Sorties').toPromise();
  }
}
