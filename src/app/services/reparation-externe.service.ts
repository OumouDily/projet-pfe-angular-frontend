import { Injectable } from '@angular/core';
import { ReparationExterne } from '../entities/reparation-externe';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { LReparationE } from '../entities/lreparation-e';

@Injectable({
  providedIn: 'root'
})
export class ReparationExterneService {

  formData:ReparationExterne;
  LreparationEList:LReparationE[];

  constructor(private http:HttpClient) { }

  saveOrUpdate(){
    var body={
      ...this.formData,
      LReparationE: this.LreparationEList
    };
    return this.http.post(environment.apiURL+'/ReparationExternes',body);
  }
  getReparationList(){
    return this.http.get(environment.apiURL+'/ReparationExternes').toPromise();
  }
  getReparationByID(id:number):any{
    return this.http.get(environment.apiURL+'/ReparationExternes'+id).toPromise();
  }
  deleteReparation(id:number){
    return this.http.delete(environment.apiURL+'/ReparationExternes/'+id).toPromise();
  }
}
