import { Injectable } from '@angular/core';
import { Affectation } from '../entities/affectation';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AffectationService {

  formData:Affectation;
 
  affectationList:Affectation[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient) { }
  PostAffectation(formData:Affectation){
   return this.http.post(this.rootURL+'/Affectations',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Affectations').toPromise().then(res => this.affectationList= res as Affectation[]);

  }
  PutAffectation(formData:Affectation){
    return this.http.put(this.rootURL+'/Affectations/'+formData.IdAff,formData);
   }
   DeleteAffectation(id:number)
   {
    return this.http.delete(this.rootURL+'/Affectations/'+id);
   }
   getAffectationList(){
    return this.http.get(environment.apiURL+'/Affectations').toPromise();
  }
}
