import { Injectable } from '@angular/core';
import { TypeEngin } from '../entities/typeEngin';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ModelService } from './model.service';

@Injectable({
  providedIn: 'root'
})
export class TypeEnginsService {

  formData:TypeEngin;
 
  typeList:TypeEngin[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient,private modelService:ModelService) { }
  PostTypeEngin(formData:TypeEngin){
   return this.http.post(this.rootURL+'/TypeEngins',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/TypeEngins').toPromise().then(res => this.typeList= res as TypeEngin[]);

  }
  PutTypeEngin(formData:TypeEngin){
    return this.http.put(this.rootURL+'/TypeEngins/'+formData.Id,formData);
   }
   DeleteTypeEngin(id:number)
   {
    return this.http.delete(this.rootURL+'/TypeEngins/'+id);
   }
   getTypeList(){
    return this.http.get(environment.apiURL+'/TypeEngins').toPromise();
  }
}

