import { Injectable } from '@angular/core';
import { Entretien } from '../entities/entretien';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EntretienService {

  formData:Entretien;
  list:Entretien[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient) { }
  PostEntretien(formData:Entretien){
   return this.http.post(this.rootURL+'/Entretiens',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Entretiens').toPromise().then(res => this.list= res as Entretien[]);

  }
  PutEntretien(formData:Entretien){
    return this.http.put(this.rootURL+'/Entretiens/'+formData.IdEnt,formData);
   }
   DeleteEntretien(id:number)
   {
    return this.http.delete(this.rootURL+'/Entretiens/'+id);
   }

}
