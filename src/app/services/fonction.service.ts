import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Fonction } from '../entities/fonction';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FonctionService {

 
  formData:Fonction;
 
  fonctionList:Fonction[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient) { }
  PostFonction(formData:Fonction){
   return this.http.post(this.rootURL+'/Fonctions',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Fonctions').toPromise().then(res => this.fonctionList= res as Fonction[]);

  }
  PutFonction(formData:Fonction){
    return this.http.put(this.rootURL+'/Fonctions/'+formData.IdF,formData);
   }
   DeleteFonction(id:number)
   {
    return this.http.delete(this.rootURL+'/Fonctions/'+id);
   }
   getFonctionList(){
    return this.http.get(environment.apiURL+'/Fonctions').toPromise();
  }


}
