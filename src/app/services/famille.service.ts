import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { HttpClient } from '@angular/common/http';
import { Famille } from '../entities/famille';
import { environment } from 'src/environments/environment';




@Injectable({
  providedIn: 'root'
})
export class FamilleService {
  

  formData:Famille;
  listfam:Famille[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient) { }
  PostFamille(formData:Famille){
   return this.http.post(this.rootURL+'/Familles',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Familles').toPromise().then(res => this.listfam= res as Famille[]);

  }
  PutFamille(formData:Famille){
    return this.http.put(this.rootURL+'/Familles/'+formData.IdFa,formData);
   }
   DeleteFamille(id:number)
   {
    return this.http.delete(this.rootURL+'/Familles/'+id);
   }
   getFamilleList(){
    return this.http.get(environment.apiURL+'/Familles').toPromise();
  }

}
