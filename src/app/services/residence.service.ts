import { Injectable } from '@angular/core';
import { Residence } from '../entities/residence';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ResidenceService {

  formData:Residence;
  ResidenceList:Residence[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient) { }
  PostResidence(formData:Residence){
   return this.http.post(this.rootURL+'/Residences',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Residences').toPromise().then(res => this.ResidenceList= res as Residence[]);

  }
  PutResidence(formData:Residence){
    return this.http.put(this.rootURL+'/Residences/'+formData.IdR,formData);
   }
   DeleteResidence(id:number)
   {
    return this.http.delete(this.rootURL+'/Residences/'+id);
   }
   getResidenceList(){
    return this.http.get(environment.apiURL+'/Residences').toPromise();
  }
}
