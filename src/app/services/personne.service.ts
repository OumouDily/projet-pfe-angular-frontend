import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Personne } from '../entities/personne';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class PersonneService {

  formData:Personne;
  listPersonne:Personne[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient) { }
  PostPersonne(formData:Personne){
   return this.http.post(this.rootURL+'/Personnes',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Personnes').toPromise().then(res => this.listPersonne= res as Personne[]);

  }
  PutPersonne(formData:Personne){
    return this.http.put(this.rootURL+'/Personnes/'+formData.IdP,formData);
   }
   DeletePersonne(id:number)
   {
    return this.http.delete(this.rootURL+'/Personnes/'+id);
   }

   getPersonneList(){
    return this.http.get(environment.apiURL+'/Personnes').toPromise();
  }
}
