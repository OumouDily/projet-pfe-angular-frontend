import { Injectable } from '@angular/core';
import { Motif } from '../entities/motif';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MotifService {

  formData:Motif;
 
  motifList:Motif[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient,private motifService:MotifService) { }
  PostMotif(formData:Motif){
   return this.http.post(this.rootURL+'/Motifs',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Motifs').toPromise().then(res => this.motifList= res as Motif[]);

  }
  PutMotif(formData:Motif){
    return this.http.put(this.rootURL+'/Motifs/'+formData.Id,formData);
   }
   DeleteMotif(id:number)
   {
    return this.http.delete(this.rootURL+'/Motifs/'+id);
   }
   getMotifList(){
    return this.http.get(environment.apiURL+'/Motifs').toPromise();
  }
}
