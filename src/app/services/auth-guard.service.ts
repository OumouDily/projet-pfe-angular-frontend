import { Injectable } from '@angular/core';
import { CanActivate, Router,ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import {JwtHelperService} from '@auth0/angular-jwt'

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    const jwtHelperService = new JwtHelperService();

    if(!jwtHelperService.isTokenExpired(sessionStorage.getItem('token'))) {
      return true;
    }

this.router.navigate(['/Login']);

    return false;
  }
  
  constructor(private router: Router) { }
 

}
