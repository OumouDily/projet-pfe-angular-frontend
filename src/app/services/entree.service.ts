import { Injectable } from '@angular/core';
import { Entree } from '../entities/entree';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EntreeService {

  formData:Entree;
 
  entreeList:Entree[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient,private entreeService:EntreeService) { }
  PostEntree(formData:Entree){
   return this.http.post(this.rootURL+'/Entrees',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Entrees').toPromise().then(res => this.entreeList= res as Entree[]);

  }
  PutEntree(formData:Entree){
    return this.http.put(this.rootURL+'/Entrees/'+formData.IdEn,formData);
   }
   DeleteEntree(id:number)
   {
    return this.http.delete(this.rootURL+'/Entrees/'+id);
   }
   getEntreeList(){
    return this.http.get(environment.apiURL+'/Entrees').toPromise();
  }
}
