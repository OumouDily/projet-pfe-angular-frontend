import { Injectable } from '@angular/core';
import { Reparation } from '../entities/reparation';
import { LReparation } from '../entities/l-reparation';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReparationService {

  formData:Reparation;
  LreparationList:LReparation[];

  constructor(private http:HttpClient) { }

  saveOrUpdate(){
    var body={
      ...this.formData,
      LReparation: this.LreparationList
    };
    return this.http.post(environment.apiURL+'/Reparations',body);
  }
  getReparationList(){
    return this.http.get(environment.apiURL+'/Reparations').toPromise();
  }
  getReparationByID(id:number):any{
    return this.http.get(environment.apiURL+'/Reparations'+id).toPromise();
  }
  deleteReparation(id:number){
    return this.http.delete(environment.apiURL+'/Reparations/'+id).toPromise();
  }
}
