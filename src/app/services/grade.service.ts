import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Grade } from '../entities/grade';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GradeService {

  formData:Grade;
 
  gradeList:Grade[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient) { }
  PostGrade(formData:Grade){
   return this.http.post(this.rootURL+'/Grades',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Grades').toPromise().then(res => this.gradeList= res as Grade[]);

  }
  PutGrade(formData:Grade){
    return this.http.put(this.rootURL+'/Grades/'+formData.IdG,formData);
   }
   DeleteGrade(id:number)
   {
    return this.http.delete(this.rootURL+'/Grades/'+id);
   }
   getGradeList(){
    return this.http.get(environment.apiURL+'/Grades').toPromise();
  }

}
