import { Injectable } from '@angular/core';
import { Mission } from '../entities/mission';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MissionService {

  formData:Mission;
 
  missionList:Mission[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient,private missionService:MissionService) { }
  PostMission(formData:Mission){
   return this.http.post(this.rootURL+'/Missions',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Missions').toPromise().then(res => this.missionList= res as Mission[]);

  }
 
  PutMission(formData:Mission){
    return this.http.put(this.rootURL+'/Missions/'+formData.IdMi,formData);
   }
   DeleteMission(id:number)
   {
    return this.http.delete(this.rootURL+'/Missions/'+id);
   }
   getMissionList(){
    return this.http.get(environment.apiURL+'/Missions').toPromise();
  }
  getMission() {
    return this.http.delete(this.rootURL+'/Missions/');
  }
}
