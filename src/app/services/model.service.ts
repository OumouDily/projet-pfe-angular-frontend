import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MarqueService } from './marque.service';
import { Model } from '../entities/model';


@Injectable({
  providedIn: 'root'
})
export class ModelService {

  formData:Model;
 
  modelList:Model[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient,private marqueService:MarqueService) { }
  PostModel(formData:Model){
   return this.http.post(this.rootURL+'/Models',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Models').toPromise().then(res => this.modelList= res as Model[]);

  }
  PutModel(formData:Model){
    return this.http.put(this.rootURL+'/Models/'+formData.IdM,formData);
   }
   DeleteModel(id:number)
   {
    return this.http.delete(this.rootURL+'/Models/'+id);
   }
   getModelList(){
    return this.http.get(environment.apiURL+'/Models').toPromise();
  }


}
