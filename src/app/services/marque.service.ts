import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Marque } from '../entities/marque';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';




@Injectable({
  providedIn: 'root'
})
export class MarqueService {
  

  formData:Marque;

  marqueList:Marque[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient) { }
  PostMarque(formData:Marque){
   return this.http.post(this.rootURL+'/Marques',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Marques').toPromise().then(res => this.marqueList= res as Marque[]);

  }
  PutMarque(formData:Marque){
    return this.http.put(this.rootURL+'/Marques/'+formData.IdMa,formData);
   }
   DeleteMarque(id:number)
   {
    return this.http.delete(this.rootURL+'/Marques/'+id);
   }
   getMarqueList(){
    return this.http.get(environment.apiURL+'/Marques').toPromise();
  }


}
