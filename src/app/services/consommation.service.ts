import { Injectable } from '@angular/core';
import { Consommation } from '../entities/consommation';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConsommationService {

  formData:Consommation;
 
  consommationList:Consommation[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient) { }
  PostConsommation(formData:Consommation){
   return this.http.post(this.rootURL+'/Consommations',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Consommations').toPromise().then(res => this.consommationList= res as Consommation[]);

  }
  PutConsommation(formData:Consommation){
    return this.http.put(this.rootURL+'/Consommations/'+formData.Id,formData);
   }
   DeleteConsommation(id:number)
   {
    return this.http.delete(this.rootURL+'/Consommations/'+id);
   }
   getConsommationList(){
    return this.http.get(environment.apiURL+'/Consommations').toPromise();
  }
}
