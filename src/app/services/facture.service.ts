import { Injectable } from '@angular/core';
import { Facture } from '../entities/facture';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FactureService {

  formData:Facture;
  listFacture:Facture[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient) { }
  GetFacture(id:number){
    return this.http.get(this.rootURL+'/Factures'+id);
   

  }
  PostFacture(formData:Facture){
   return this.http.post(this.rootURL+'/Factures',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Factures').toPromise().then(res => this.listFacture= res as Facture[]);

  }
  PutFacture(formData:Facture){
    return this.http.put(this.rootURL+'/Factures/'+formData.Id,formData);
   }
   DeleteFacture(id:number)
   {
    return this.http.delete(this.rootURL+'/Factures/'+id);
   }
  
}
