import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StatistiqueService {
  readonly rootURL="http://localhost:63003/api";
  constructor(private httpClient: HttpClient) { }

  public getConsomationByMonth(): Observable<any> {
    return this.httpClient.get(this.rootURL+'/Consommations/GetConsomMonthlyByDirection')
  }
  public getConsomationByNature(): Observable<any> {
    return this.httpClient.get(this.rootURL+'/Consommations/GetConsomMonthlyByNature')
  }
}
