import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Carburant } from '../entities/carburant';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CarburantService {

  formData:Carburant;
  list:Carburant[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient) { }
  PostCarburant(formData:Carburant){
   return this.http.post(this.rootURL+'/Carburants',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Carburants').toPromise().then(res => this.list= res as Carburant[]);

  }
  PutCarburant(formData:Carburant){
    return this.http.put(this.rootURL+'/Carburants/'+formData.IdC,formData);
   }
   DeleteCarburant(id:number)
   {
    return this.http.delete(this.rootURL+'/Carburants/'+id);
   }


}
