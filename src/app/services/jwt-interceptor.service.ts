import { Injectable } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable({
  providedIn: 'root'
})
export class JwtInterceptorService implements HttpInterceptor {
  intercept(req: import("@angular/common/http").HttpRequest<any>, next: import("@angular/common/http").HttpHandler): import("rxjs").Observable<import("@angular/common/http").HttpEvent<any>> {
 
    const jwtHelperService = new JwtHelperService();

    if ( !jwtHelperService.isTokenExpired(sessionStorage.getItem('token'))) {
      req = req.clone ( {
        setHeaders : {
          Authorization : sessionStorage.getItem('token')
        }
      })
    }
    return next.handle(req);
  }

  constructor() { }

}
