import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Produit } from '../entities/produit';


@Injectable({
  providedIn: 'root'
})
export class ProduitService {

  formData:Produit = new Produit();
  listProduit:Produit[];
  readonly rootURL="http://localhost:63003/api";

  constructor(private http:HttpClient) { }
  
  getProduitList(){
    return this.http.get(environment.apiURL+'/Produits').toPromise();
  }
  PostProduit(formData:Produit){
    return this.http.post(this.rootURL+'/Produits',formData);
   }
   refreshList(){
     this.http.get(this.rootURL+'/Produits').toPromise().then(res => this.listProduit= res as Produit[]);
 
   }
   PutProduit(formData:Produit){
     return this.http.put(this.rootURL+'/Produits/'+formData.IdP,formData);
    }
    DeleteProduit(id:number)
    {
     return this.http.delete(this.rootURL+'/Produits/'+id);
    }
 
}
