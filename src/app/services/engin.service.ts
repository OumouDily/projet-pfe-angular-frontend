import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Engin } from '../entities/engin';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MarqueService } from './marque.service';
import { ModelService } from './model.service';

import { Marque } from '../entities/marque';
import { Model } from '../entities/model';
import { Type } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class EnginService {

  formData:Engin;
 
  enginList:Engin[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient,private marqueService:MarqueService) { }
  PostEngin(formData:Engin){
   return this.http.post(this.rootURL+'/Engins',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Engins').toPromise().then(res => this.enginList= res as Engin[]);

  }
  PutEngin(formData:Engin){
    return this.http.put(this.rootURL+'/Engins/'+formData.IdE,formData);
   }
   DeleteEngin(id:number)
   {
    return this.http.delete(this.rootURL+'/Engins/'+id);
   }
   getEnginList(){
    return this.http.get(environment.apiURL+'/Engins').toPromise();
  }
  getEnginByID(id:number):any{
    return this.http.get(environment.apiURL+'/Engins'+id).toPromise();
  }
 

}
