import { Injectable } from '@angular/core';
import { Sfamille } from '../entities/sfamille';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SfamilleService {

 
  formData:Sfamille;
  listsfam:Sfamille[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient) { }
  PostSfamille(formData:Sfamille){
   return this.http.post(this.rootURL+'/Sfamilles',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Sfamilles').toPromise().then(res => this.listsfam= res as Sfamille[]);

  }
  PutSfamille(formData:Sfamille){
    return this.http.put(this.rootURL+'/Sfamilles/'+formData.IdSf,formData);
   }
   DeleteSfamille(id:number)
   {
    return this.http.delete(this.rootURL+'/Sfamilles/'+id);
   }
   getSfamilleList(){
    return this.http.get(environment.apiURL+'/Sfamilles').toPromise();
  }

}
