import { Injectable } from '@angular/core';
import { DemandeMission } from '../entities/demande-mission';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DemandeService {

  formData:DemandeMission;
 
  demandeList:DemandeMission[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient,private demandeService:DemandeService) { }
  PostDemandeMission(formData:DemandeMission){
   return this.http.post(this.rootURL+'/DemandeMissions',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/DemandeMissions').toPromise().then(res => this.demandeList= res as DemandeMission[]);

  }
  PutDemandeMission(formData:DemandeMission){
    return this.http.put(this.rootURL+'/DemandeMissions/'+formData.IdDM,formData);
   }
   DeleteDemandeMission(id:number)
   {
    return this.http.delete(this.rootURL+'/DemandeMissions/'+id);
   }
   getDemandeList(){
    return this.http.get(environment.apiURL+'/DemandeMissions').toPromise();
  }
}
