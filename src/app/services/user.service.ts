import { Injectable } from '@angular/core';


import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { User } from '../entities/user';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  isLogin:boolean;
  formData:User;
  userList : User[];
  readonly rootURL="http://localhost:63003/api";
  constructor(private http:HttpClient) { }

 
  PostUser(formData:User){
   return this.http.post(this.rootURL+'/Users',formData);
  }
  refreshList(){
    this.http.get(this.rootURL+'/Users').toPromise().then(res => this.userList= res as User[]);

  }
 

  DeleteUser(id:number)
  {
   return this.http.delete(this.rootURL+'/Users/'+id);
  }

 
}
