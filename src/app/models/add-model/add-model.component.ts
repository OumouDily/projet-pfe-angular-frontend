import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';  
import { FormBuilder, FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { ModelService } from 'src/app/services/model.service';

import { Marque } from 'src/app/entities/marque';
import { MarqueService } from 'src/app/services/marque.service';


@Component({
  selector: 'app-add-model',
  templateUrl: './add-model.component.html',
  styleUrls: ['./add-model.component.css']
})
export class AddModelComponent implements OnInit {

  marqueList:Marque[];
  constructor(private modelService:ModelService,private marqueService:MarqueService,private toastr:ToastrService) { }

  ngOnInit() {
    this.resetForm();
    this.marqueService.getMarqueList().then(res => this.marqueList = res as Marque[]);
  }
  resetForm(form? : NgForm){
    if(form!=null)
   form.resetForm();
   this.modelService.formData={
     IdM:null,
     LibelleModel:'',
     Id_Marque:6
    
   }
  }
  onSubmit(form: NgForm){
    if(form.value.IdM==null)
    this.insertModel(form);
    else
    this.updateModel(form);
  }
  insertModel(form: NgForm){
    this.modelService.PostModel(form.value).subscribe(res => {
    this.toastr.success('Insertion effectuée avec succès!');
   
    this.resetForm(form);
    this.modelService.refreshList();
    });
  }
  updateModel(form: NgForm){
    this.modelService.PutModel(form.value)
    .subscribe(res => {
      this.toastr.info('Modification effectuée!');
     
      this.resetForm(form);
      this.modelService.refreshList();
      });
  }

}
