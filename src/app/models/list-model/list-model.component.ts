import { Component, OnInit } from '@angular/core';
import { ModelService } from 'src/app/services/model.service';
import { ToastrService } from 'ngx-toastr';
import { Model } from 'src/app/entities/model';

@Component({
  selector: 'app-list-model',
  templateUrl: './list-model.component.html',
  styleUrls: ['./list-model.component.css']
})
export class ListModelComponent implements OnInit {

  constructor(private modelService:ModelService,private toastr:ToastrService) { }

  ngOnInit() {
    this.modelService.refreshList();
  }
  populateForm(m:Model){
    this.modelService.formData=Object.assign({},m);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.modelService.DeleteModel(id).subscribe(res=>{
      this.modelService.refreshList();
      this.toastr.warning('Suppression effectuée!');
     
    })
  }
}
}
