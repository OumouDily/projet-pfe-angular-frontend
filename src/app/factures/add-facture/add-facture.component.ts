import { Component, OnInit } from '@angular/core';
import { FactureService } from 'src/app/services/facture.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { Fournisseur } from 'src/app/entities/fournisseur';
import { FournisseurService } from 'src/app/services/fournisseur.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-add-facture',
  templateUrl: './add-facture.component.html',
  styleUrls: ['./add-facture.component.css']
})
export class AddFactureComponent implements OnInit {

  minDate  ;
  fournisseurList:Fournisseur[];
   constructor(private fournisseurService:FournisseurService,private factureService:FactureService,private toastr:ToastrService,private datePipe: DatePipe) { }
 
   ngOnInit() {
    this.minDate = this.transformDate(new Date());
     this.resetForm();
     this.fournisseurService.getFournisseurList().then(res => this.fournisseurList = res as Fournisseur[]);
    
    
   }
   transformDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }
   resetForm(form? : NgForm){
     if(form!=null)
    form.resetForm();
    this.factureService.formData={
      Id:null,
      Date_Facture:'',
      Fournisseur:0,
    
     Observation:'',
     TotHt:0,
  
      TotRem:0,
      TotTVA:0,
      TotTTC:0,
      Timbre:'',
      NET:0
 
 
     
    }
   }
   onSubmit(form: NgForm){
     if(form.value.Id==null)
     this.insertFacture(form);
     else
     this.updateFacture(form);
   }
   insertFacture(form: NgForm){
     this.factureService.PostFacture(form.value).subscribe(res => {
     this.toastr.success('Insertion effectuée avec succès!');
   
     this.resetForm(form);
     this.factureService.refreshList();
     });
   }
   updateFacture(form: NgForm){
     this.factureService.PutFacture(form.value)
     .subscribe(res => {
       this.toastr.info('Modification effectuée!');
      
       this.resetForm(form);
       this.factureService.refreshList();
       });
   }
 

}
