import { Component, OnInit } from '@angular/core';
import { FactureService } from 'src/app/services/facture.service';
import { ToastrService } from 'ngx-toastr';
import { Facture } from 'src/app/entities/facture';
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';



@Component({
  selector: 'app-list-facture',
  templateUrl: './list-facture.component.html',
  styleUrls: ['./list-facture.component.css']
})
export class ListFactureComponent implements OnInit {

  constructor(private factureService:FactureService,private toastr:ToastrService) { }

  ngOnInit() {
    this.factureService.refreshList();
  }
 
  populateForm(f:Facture){
    this.factureService.formData=Object.assign({},f);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.factureService.DeleteFacture(id).subscribe(res=>{
      this.factureService.refreshList();
      this.toastr.warning('Suppression effectuée!');
     
    })
  }
}

}
