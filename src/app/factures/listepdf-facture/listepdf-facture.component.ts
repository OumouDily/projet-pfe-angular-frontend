import { Component, OnInit } from '@angular/core';
import { FactureService } from 'src/app/services/facture.service';
import { ToastrService } from 'ngx-toastr';
import { Facture } from 'src/app/entities/facture';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-listepdf-facture',
  templateUrl: './listepdf-facture.component.html',
  styleUrls: ['./listepdf-facture.component.css']
})
export class ListepdfFactureComponent implements OnInit {

  constructor(private factureService:FactureService,private toastr:ToastrService) { }

  ngOnInit() {
    this.factureService.refreshList();
  }
 
  populateForm(f:Facture){
    this.factureService.formData=Object.assign({},f);
  }
generatePdf() {
 
    const div = document.getElementById("Facture");
    const options = {background: "white", height: div.clientHeight, width: div.clientWidth};
  
    html2canvas(div, options).then((canvas) => {
  
        
        //Initialize JSPDF
        let doc = new jsPDF("p", "mm", "a4");
     
        //Converting canvas to Image
        let imgData = canvas.toDataURL("image/PNG");
  
        
        //Add image Canvas to PDF
        doc.addImage(imgData, 'PNG',5,0,190,100);
  
        let pdfOutput = doc.output();
        // using ArrayBuffer will allow you to put image inside PDF
        let buffer = new ArrayBuffer(pdfOutput.length);
        let array = new Uint8Array(buffer);
        for (let i = 0; i < pdfOutput.length; i++) {
            array[i] = pdfOutput.charCodeAt(i);
        }
  
        //Name of pdf
        const fileName = "ListeFatcure.pdf";
  
        // Make file
        doc.save(fileName);
  
    });
  }

}
