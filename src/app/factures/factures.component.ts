import { Component, OnInit } from '@angular/core';
import { FactureService } from '../services/facture.service';

@Component({
  selector: 'app-factures',
  templateUrl: './factures.component.html',
  styleUrls: ['./factures.component.css']
})
export class FacturesComponent implements OnInit {

  constructor(private factureService:FactureService) { }

  ngOnInit() {
  }

}
