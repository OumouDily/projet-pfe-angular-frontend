import { Component, OnInit } from '@angular/core';
import { FournisseurService } from 'src/app/services/fournisseur.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-fournisseur',
  templateUrl: './add-fournisseur.component.html',
  styleUrls: ['./add-fournisseur.component.css']
})
export class AddFournisseurComponent implements OnInit {

  patternLogin='^[a-z0-9_-]{8,15}$';
  patternEmail='^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$';
  patternPhone='^((\\+91-?)|0)?[0-9]{10}$';
  patternPwd = '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$';
  constructor(private fournisseurService:FournisseurService,private toastr:ToastrService) { }
 
   ngOnInit() {
     this.resetForm();
   
   }
   resetForm(form? : NgForm){
     if(form!=null)
    form.resetForm();
    this.fournisseurService.formData={
      IdFo:null,
      LibelleFournisseur:'',
    
      Responsable:'',
  
      Adresse:'',
      Ville:'',
      Tel:0,
      Email:'',
      Login:'',
      Mdp:''
 
 
     
    }
   }
   onSubmit(form: NgForm){
     if(form.value.IdFo==null)
     this.insertFournisseur(form);
     else
     this.updateFournisseur(form);
   }
   insertFournisseur(form: NgForm){
     this.fournisseurService.PostFournisseur(form.value).subscribe(res => {
     this.toastr.success('Insertion effectuée avec succès!');
     
     this.resetForm(form);
     this.fournisseurService.refreshList();
     });
   }
   updateFournisseur(form: NgForm){
     this.fournisseurService.PutFournisseur(form.value)
     .subscribe(res => {
       this.toastr.info('Modification effectuée!');
    
       this.resetForm(form);
       this.fournisseurService.refreshList();
       });
   }
 


}
