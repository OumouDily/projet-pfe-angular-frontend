import { Component, OnInit } from '@angular/core';
import { FournisseurService } from 'src/app/services/fournisseur.service';
import { ToastrService } from 'ngx-toastr';
import { Fournisseur } from 'src/app/entities/fournisseur';

@Component({
  selector: 'app-list-fournisseur',
  templateUrl: './list-fournisseur.component.html',
  styleUrls: ['./list-fournisseur.component.css']
})
export class ListFournisseurComponent implements OnInit {

  constructor(private fournisseurService:FournisseurService,private toastr:ToastrService) { }

  ngOnInit() {
    this.fournisseurService.refreshList();
  }
  populateForm(f:Fournisseur){
    this.fournisseurService.formData=Object.assign({},f);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.fournisseurService.DeleteFournisseur(id).subscribe(res=>{
      this.fournisseurService.refreshList();
      this.toastr.warning('Suppression effectuée!');
     
    })
  }
}

}
