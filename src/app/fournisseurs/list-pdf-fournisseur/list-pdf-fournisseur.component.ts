import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FournisseurService } from 'src/app/services/fournisseur.service';
import { Fournisseur } from 'src/app/entities/fournisseur';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-list-pdf-fournisseur',
  templateUrl: './list-pdf-fournisseur.component.html',
  styleUrls: ['./list-pdf-fournisseur.component.css']
})
export class ListPdfFournisseurComponent implements OnInit {

  constructor(private fournisseurService:FournisseurService,private toastr:ToastrService) { }

  ngOnInit() {
    this.fournisseurService.refreshList();
  }
  populateForm(f:Fournisseur){
    this.fournisseurService.formData=Object.assign({},f);
  }
 
  generatePdf() {
 
    const div = document.getElementById("Fournisseur");
    const options = {background: "white", height: div.clientHeight, width: div.clientWidth};
  
    html2canvas(div, options).then((canvas) => {
  
        
        //Initialize JSPDF
        let doc = new jsPDF("p", "mm", "a4");
     
        //Converting canvas to Image
        let imgData = canvas.toDataURL("image/PNG");
  
        
        //Add image Canvas to PDF
        doc.addImage(imgData, 'PNG',5,0,190,100);
  
        let pdfOutput = doc.output();
        // using ArrayBuffer will allow you to put image inside PDF
        let buffer = new ArrayBuffer(pdfOutput.length);
        let array = new Uint8Array(buffer);
        for (let i = 0; i < pdfOutput.length; i++) {
            array[i] = pdfOutput.charCodeAt(i);
        }
  
        //Name of pdf
        const fileName = "ListeFournisseur.pdf";
  
        // Make file
        doc.save(fileName);
  
    });
  }

}
