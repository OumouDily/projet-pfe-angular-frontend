import { Component, OnInit } from '@angular/core';
import { FournisseurService } from '../services/fournisseur.service';

@Component({
  selector: 'app-fournisseurs',
  templateUrl: './fournisseurs.component.html',
  styleUrls: ['./fournisseurs.component.css']
})
export class FournisseursComponent implements OnInit {

  constructor(private fournisseurService:FournisseurService) { }

  ngOnInit() {
  }

}
