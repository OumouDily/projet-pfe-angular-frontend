import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { TypeEngin } from 'src/app/entities/typeEngin';
import { TypeEnginsService } from 'src/app/services/type-engins.service';


@Component({
  selector: 'app-list-type',
  templateUrl: './list-type.component.html',
  styleUrls: ['./list-type.component.css']
})
export class ListTypeComponent implements OnInit {

  constructor(private typeService:TypeEnginsService,private toastr:ToastrService) { }

  ngOnInit() {
    this.typeService.refreshList();
  }
  populateForm(t:TypeEngin){
    this.typeService.formData=Object.assign({},t);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.typeService.DeleteTypeEngin(id).subscribe(res=>{
      this.typeService.refreshList();
      this.toastr.warning('Suppression effectuée!');
   
    })
  }
}

}
