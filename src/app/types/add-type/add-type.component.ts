import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder, NgForm } from '@angular/forms';
import { Model } from 'src/app/entities/model';
import { ModelService } from 'src/app/services/model.service';
import { ToastrService } from 'ngx-toastr';
import { TypeEnginsService } from 'src/app/services/type-engins.service';
import { TypeEngin } from 'src/app/entities/typeEngin';

@Component({
  selector: 'app-add-type',
  templateUrl: './add-type.component.html',
  styleUrls: ['./add-type.component.css']
})
export class AddTypeComponent implements OnInit {

 // modelList:Model;
 modelList:Model[];
 constructor(private modelService:ModelService,private typeService:TypeEnginsService,private toastr:ToastrService) { }

 ngOnInit() {
   this.resetForm();
   this.modelService.getModelList().then(res => this.modelList = res as Model[]);
 }
 resetForm(form? : NgForm){
   if(form!=null)
  form.resetForm();
  this.typeService.formData={
    Id:null,
    LibelleTypeEngin:'',
    Id_Model:0
   
  }
 }
 onSubmit(form: NgForm){
   if(form.value.Id==null)
   this.insertType(form);
   else
   this.updateType(form);
 }
 insertType(form: NgForm){
   this.typeService.PostTypeEngin(form.value).subscribe(res => {
   this.toastr.success('Insertion effectuée avec succès!');
  
   this.resetForm(form);
   this.typeService.refreshList();
   });
 }
 updateType(form: NgForm){
   this.typeService.PutTypeEngin(form.value)
   .subscribe(res => {
    this.toastr.info('Modification effectuée!');
    
     this.resetForm(form);
     this.typeService.refreshList();
     });
 }

}
