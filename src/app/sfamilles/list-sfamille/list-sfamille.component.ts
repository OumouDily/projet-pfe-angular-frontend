import { Component, OnInit } from '@angular/core';
import { SfamilleService } from 'src/app/services/sfamille.service';
import { ToastrService } from 'ngx-toastr';
import { Sfamille } from 'src/app/entities/sfamille';

@Component({
  selector: 'app-list-sfamille',
  templateUrl: './list-sfamille.component.html',
  styleUrls: ['./list-sfamille.component.css']
})
export class ListSfamilleComponent implements OnInit {

  constructor(private sfamilleService:SfamilleService,private toastr:ToastrService) { }

  ngOnInit() {
    this.sfamilleService.refreshList();
  }
  populateForm(f:Sfamille){
    this.sfamilleService.formData=Object.assign({},f);
  }
  onDelete(id:number){
    if(confirm('Etes-vous sure de vouloir supprimer?')){
    this.sfamilleService.DeleteSfamille(id).subscribe(res=>{
      this.sfamilleService.refreshList();
      this.toastr.warning('Suppression effectuée!');
     
    })
  }
}

}
