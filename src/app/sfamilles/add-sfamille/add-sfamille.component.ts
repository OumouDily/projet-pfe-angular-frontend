import { Component, OnInit } from '@angular/core';
import { SfamilleService } from 'src/app/services/sfamille.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { FamilleService } from 'src/app/services/famille.service';
import { Famille } from 'src/app/entities/famille';

@Component({
  selector: 'app-add-sfamille',
  templateUrl: './add-sfamille.component.html',
  styleUrls: ['./add-sfamille.component.css']
})
export class AddSfamilleComponent implements OnInit {
  sfamilleList:Famille[];
  constructor(private sfamilleService:SfamilleService,private familleService:FamilleService,private toastr:ToastrService) { }

  ngOnInit() {
    this.resetForm();
    this.familleService.getFamilleList().then(res => this.sfamilleList = res as Famille[]);
  }
  resetForm(form? : NgForm){
    if(form!=null)
   form.resetForm();
   this.sfamilleService.formData={
     IdSf:null,
     LibelleSFamille:'',
     Id_Famille:0
    
   }
  }
  onSubmit(form: NgForm){
    if(form.value.IdFa==null)
    this.insertSfamille(form);
    else
    this.updateSfamille(form);
  }
  insertSfamille(form: NgForm){
    this.sfamilleService.PostSfamille(form.value).subscribe(res => {
   this.toastr.success('Insertion effectuée avec succès!');
   
    this.resetForm(form);
    this.sfamilleService.refreshList();
    });
  }
  updateSfamille(form: NgForm){
    this.sfamilleService.PutSfamille(form.value)
    .subscribe(res => {
     this.toastr.info('Modification effectuée!');

      this.resetForm(form);
      this.sfamilleService.refreshList();
      });
  }
}
